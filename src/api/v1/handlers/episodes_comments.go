package handlers

import (
	"net/http"
	"strconv"
	"time"

	"github.com/labstack/echo/v4"
	_echo "github.com/marloncristian/oricom-goframework/web/echo"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"oricom.io/foundation/api/v1/models"
	"oricom.io/foundation/domain"
	"oricom.io/foundation/domain/events"
	"oricom.io/foundation/domain/repository"
	"oricom.io/foundation/utils"
	"oricom.io/foundation/utils/assertion"
)

func (handler *HandlerEpisodes) getEpisodeComments(c echo.Context) error {

	offset := int64(0)
	if offsetParam := c.QueryParam("_offset"); len(offsetParam) > 0 {
		offsetPar, err := strconv.ParseInt(offsetParam, 10, 64)
		if err == nil {
			offset = offsetPar
		}
	}

	limit := int64(100)
	if limitParam := c.QueryParam("_limit"); len(limitParam) > 0 {
		limitPar, err := strconv.ParseInt(limitParam, 10, 64)
		if err == nil && limitPar < 100 {
			limit = limitPar
		}
	}

	novelid := c.Param("novelid")
	episodeid := c.Param("episodeid")

	assert := assertion.Assert{}
	failure := assert.Novel().Exists(novelid).Root().Episode().Exists(episodeid).Root().Assert()
	if failure != nil {
		if _, ok := failure.(assertion.NovelDoesNotExistsError); ok {
			return c.NoContent(http.StatusNotFound)
		}
		if _, ok := failure.(assertion.EpisodeDoesNotExistsError); ok {
			return c.NoContent(http.StatusNotFound)
		}
		return utils.InternalServerError(c, failure)
	}

	episode := assert.GetEntity("episode", episodeid).(*domain.NovelEpisodeEntity)

	comments, err := handler.CommmentRepository.GetByEpisodeID(episode.ID.Hex(), offset, limit)
	if err != nil {
		return utils.InternalServerError(c, err)
	}

	if len(comments) == 0 {
		return utils.NoContent(c)
	}

	result := []models.GetEpisodesCommentsModel{}
	for _, comment := range comments {
		result = append(result, handler.episodeCommentToGetModel(comment))
	}

	go func() {
		service := repository.NewEpisodesViews()
		_, err := service.Create(c.RealIP())
		if err != nil {
			handler.LogRepository.Error(err.Error())
		}
	}()

	return c.JSON(http.StatusOK, result)
}

func (handler *HandlerEpisodes) postEpisodeComments(c echo.Context) error {

	model := models.PostEpisodesCommentsModel{}
	if err := c.Bind(&model); err != nil {
		return utils.InternalServerError(c, err)
	}

	token, err := _echo.GetTokenFromHeader(c)
	if err != nil {
		return utils.InternalServerError(c, err)
	}

	novelid := c.Param("novelid")
	episodeid := c.Param("episodeid")

	assert := assertion.Assert{}
	failure := assert.Novel().Exists(novelid).Root().Episode().Exists(episodeid).Root().Assert()
	if failure != nil {
		if _, ok := failure.(assertion.NovelDoesNotExistsError); ok {
			return c.NoContent(http.StatusNotFound)
		}
		if _, ok := failure.(assertion.EpisodeDoesNotExistsError); ok {
			return c.NoContent(http.StatusNotFound)
		}
		return utils.InternalServerError(c, failure)
	}

	episode := assert.GetEntity("episode", episodeid).(*domain.NovelEpisodeEntity)
	userid, err := primitive.ObjectIDFromHex(token.Subscriber)
	if err != nil {
		return utils.InternalServerError(c, err)
	}

	commentsCount, err := handler.CommmentRepository.CountFiveMinutesComments(episode.ID, userid)
	if err != nil {
		return utils.InternalServerError(c, err)
	}
	if commentsCount >= 15 {
		return c.NoContent(http.StatusTooManyRequests)
	}

	comment := domain.NovelEpisodeCommentEntity{
		EpisodeID: episode.ID,
		Comment:   model.Comment,
		CreatedAt: time.Now(),
		User: domain.UserEntity{
			UserID: userid,
			Name:   token.Name,
		},
	}
	_, err = handler.CommmentRepository.Create(comment)
	if err != nil {
		utils.InternalServerError(c, err)
	}

	handler.Mediator.Raise(events.CommentCreatedMessage{
		EpisodeID: episode.ID,
	})

	return utils.NoContent(c)
}

func (handler *HandlerEpisodes) patchEpisodeComments(c echo.Context) error {
	return nil
}

func (handler *HandlerEpisodes) deleteEpisodeComments(c echo.Context) error {
	return nil
}

func (handler *HandlerEpisodes) episodeCommentToGetModel(entity domain.NovelEpisodeCommentEntity) models.GetEpisodesCommentsModel {
	return models.GetEpisodesCommentsModel{
		ID:        entity.ID.Hex(),
		EpisodeID: entity.EpisodeID.Hex(),
		Comment:   entity.Comment,
		CreatedAt: entity.CreatedAt,
		UpdatedAt: entity.UpdatedAt,
		User: models.GetEpisodesCommentsUserModel{
			UserID: entity.User.UserID.Hex(),
			Name:   entity.User.Name,
		},
	}
}
