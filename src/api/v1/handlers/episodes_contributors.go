package handlers

import (
	"net/http"

	"github.com/labstack/echo/v4"
	_echo "github.com/marloncristian/oricom-goframework/web/echo"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"oricom.io/foundation/api/v1/models"
	"oricom.io/foundation/domain"
	"oricom.io/foundation/domain/service"
	"oricom.io/foundation/utils"
	"oricom.io/foundation/utils/assertion"
)

func (handler HandlerEpisodes) postEpisodeContributor(c echo.Context) error {

	model := models.PostEpisodeContributorsModel{}
	if errMdl := c.Bind(&model); errMdl != nil {
		return c.JSON(http.StatusBadRequest, utils.ToEntityViolation(errMdl))
	}

	novelid := c.Param("novelid")
	episodeid := c.Param("id")

	token, err := _echo.GetTokenFromHeader(c)
	if err != nil {
		return utils.InternalServerError(c, err)
	}

	assert := assertion.Assert{}
	failure := assert.Novel().Exists(novelid).IsOwner(novelid, token.Subscriber).Root().Episode().Exists(episodeid).Root().Assert()
	if failure != nil {
		if _, ok := failure.(assertion.NovelDoesNotExistsError); ok {
			return c.NoContent(http.StatusNotFound)
		}
		if _, ok := failure.(assertion.EpisodeDoesNotExistsError); ok {
			return c.NoContent(http.StatusNotFound)
		}
		if _, ok := failure.(assertion.NovelNotOwnerError); ok {
			return c.NoContent(http.StatusForbidden)
		}
		return utils.InternalServerError(c, failure)
	}

	episode := *assert.GetEntity("episode", episodeid).(*domain.NovelEpisodeEntity)

	usersService := service.Users{}
	contributors := episode.Contributors
	modified := false
	for _, candidate := range model.Contributors {
		found := false
		for _, contributor := range contributors {
			if contributor.UserID.Hex() == candidate.ContributorID {
				found = true
				break
			}
		}
		if !found {
			user, err := usersService.GetByID(candidate.ContributorID)
			if err != nil {
				return c.NoContent(http.StatusInternalServerError)
			}
			userID, err := primitive.ObjectIDFromHex(user.ID)
			if err != nil {
				return c.NoContent(http.StatusInternalServerError)
			}
			contributors = append(contributors, domain.ContributorEntity{
				UserID: userID,
				Name:   user.Name,
				Role:   candidate.Role,
			})
			modified = true
		}
	}
	if !modified {
		return c.NoContent(http.StatusNotModified)
	}

	updateMap := make(map[string]interface{})
	updateMap["contributors"] = contributors
	updateErr := handler.EpisodeRepository.Update(episodeid, updateMap)
	if updateErr != nil {
		return c.NoContent(http.StatusInternalServerError)
	}

	return c.NoContent(http.StatusCreated)
}

func (handler HandlerEpisodes) deleteEpisodeContributor(c echo.Context) error {

	model := models.DeleteEpisodeContributorModel{}
	if errMdl := c.Bind(&model); errMdl != nil {
		return c.JSON(http.StatusBadRequest, utils.ToEntityViolation(errMdl))
	}

	novelid := c.Param("novelid")
	episodeid := c.Param("id")

	token, err := _echo.GetTokenFromHeader(c)
	if err != nil {
		return utils.InternalServerError(c, err)
	}

	assert := assertion.Assert{}
	failure := assert.Novel().Exists(novelid).IsOwner(novelid, token.Subscriber).Root().Episode().Exists(episodeid).Root().Assert()
	if failure != nil {
		if _, ok := failure.(assertion.NovelDoesNotExistsError); ok {
			return c.NoContent(http.StatusNotFound)
		}
		if _, ok := failure.(assertion.EpisodeDoesNotExistsError); ok {
			return c.NoContent(http.StatusNotFound)
		}
		if _, ok := failure.(assertion.NovelNotOwnerError); ok {
			return c.NoContent(http.StatusForbidden)
		}
		return utils.InternalServerError(c, failure)
	}

	episode := *assert.GetEntity("episode", episodeid).(*domain.NovelEpisodeEntity)

	contributors := []domain.ContributorEntity{}
	for _, contributor := range episode.Contributors {
		found := false
		for _, candidate := range model.ContributorsID {
			if contributor.UserID.Hex() == candidate {
				found = true
				break
			}
		}
		if !found {
			contributors = append(contributors, contributor)
		}
	}
	if len(contributors) != len(episode.Contributors) {
		return c.NoContent(http.StatusNotModified)
	}

	updateMap := make(map[string]interface{})
	updateMap["contributors"] = contributors
	updateErr := handler.EpisodeRepository.Update(episodeid, updateMap)
	if updateErr != nil {
		return c.NoContent(http.StatusInternalServerError)
	}

	return utils.NoContent(c)
}
