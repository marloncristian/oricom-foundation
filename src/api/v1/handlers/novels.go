package handlers

import (
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"github.com/labstack/echo/v4"
	"github.com/marloncristian/oricom-goframework/database"
	_echo "github.com/marloncristian/oricom-goframework/web/echo"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"oricom.io/foundation/api/v1/models"
	"oricom.io/foundation/domain"
	"oricom.io/foundation/domain/repository"
	"oricom.io/foundation/utils"
)

// HandlerNovels handler for users api
type HandlerNovels struct {
	trie            *utils.Trie
	NovelRepository *repository.Novels `inject:""`
	Context         *echo.Echo         `inject:""`
}

// Init initializes the web handlers
func (handler *HandlerNovels) Init() {

	//default route
	handler.Context.GET("/v1/novels", handler.getNovels, _echo.Authenticate)
	handler.Context.GET("/v1/novels/:id", handler.getNovel, _echo.Authenticate)
	handler.Context.GET("/v1/novels/suggestions", handler.getSuggestions, _echo.Authenticate)

	//producer only routes
	handler.Context.POST("/v1/novels", handler.postNovel, _echo.Authenticate, utils.ProducerRequired)
	handler.Context.PATCH("/v1/novels/:id", handler.patchNovel, _echo.Authenticate, utils.ProducerRequired)
	handler.Context.DELETE("/v1/novels/:id", handler.deleteNovel, _echo.Authenticate, utils.ProducerRequired)

	//me route
	handler.Context.GET("/v1/me/novels", handler.getMeNovels, _echo.Authenticate, utils.ProducerRequired)

	handler.initTrie()
}

// initTrie the suggestion memory cache
func (handler *HandlerNovels) initTrie() {
	titles, err := handler.NovelRepository.GetAllTitles()
	if err != nil {
		panic(err)
	}
	handler.trie = utils.NewTrie()
	for _, value := range titles {
		handler.trie.Insert(strings.ToLower(value.Title))
	}
}

// getMeNovels current user novels
func (handler HandlerNovels) getMeNovels(c echo.Context) error {
	token, err := _echo.GetTokenFromHeader(c)
	if err != nil {
		return utils.BadRequestError(c, err)
	}

	offset := int64(0)
	if offsetParam := c.QueryParam("_offset"); len(offsetParam) > 0 {
		offsetPar, err := strconv.ParseInt(offsetParam, 10, 64)
		if err == nil {
			offset = offsetPar
		}
	}

	limit := int64(100)
	if limitParam := c.QueryParam("_limit"); len(limitParam) > 0 {
		limitPar, err := strconv.ParseInt(limitParam, 10, 64)
		if err == nil && limitPar < 100 {
			limit = limitPar
		}
	}

	novels, errGet := handler.NovelRepository.GetFromOwner(token.Subscriber, offset, limit)
	if errGet != nil {
		return utils.UnauthorizedError(c, errGet)
	}
	if len(novels) == 0 {
		return utils.NoContent(c)
	}

	response := []models.GetNovelModel{}
	for _, item := range novels {
		response = append(response, handler.entityToGetModel(item))
	}

	c.Response().Header().Set("Content-Range", fmt.Sprintf("%c-%c", offset, offset+limit))
	c.Response().Header().Set("Accept-Range", string(limit))
	return c.JSON(http.StatusOK, response)
}

// getNovel return an specific novel
func (handler HandlerNovels) getNovel(c echo.Context) error {
	novelID := c.Param("id")

	novel, err := handler.NovelRepository.GetByID(novelID)
	if _, ok := err.(database.EntityNotFound); ok {
		return c.NoContent(http.StatusNotFound)
	}
	if err != nil {
		return utils.InternalServerError(c, err)
	}

	return c.JSON(http.StatusCreated, handler.entityToGetModel(novel))
}

// getNovels queries for novels
func (handler HandlerNovels) getNovels(c echo.Context) error {

	offset := int64(0)
	if offsetParam := c.QueryParam("_offset"); len(offsetParam) > 0 {
		offsetPar, err := strconv.ParseInt(offsetParam, 10, 64)
		if err == nil {
			offset = offsetPar
		}
	}

	limit := int64(100)
	if limitParam := c.QueryParam("_limit"); len(limitParam) > 0 {
		limitPar, err := strconv.ParseInt(limitParam, 10, 64)
		if err == nil && limitPar < 100 {
			limit = limitPar
		}
	}

	q := c.QueryParam("q")
	novels, errGet := handler.NovelRepository.Search(q, offset, limit)
	if errGet != nil {
		return utils.UnauthorizedError(c, errGet)
	}
	if len(novels) == 0 {
		return utils.NoContent(c)
	}

	response := []models.GetNovelModel{}
	for _, item := range novels {
		response = append(response, handler.entityToGetModel(item))
	}

	c.Response().Header().Set("Content-Range", fmt.Sprintf("%c-%c", offset, offset+limit))
	c.Response().Header().Set("Accept-Range", string(limit))
	return c.JSON(http.StatusOK, response)
}

// getSuggestions returns all title suggestions
func (handler *HandlerNovels) getSuggestions(c echo.Context) error {
	q := c.QueryParam("q")
	values, errS := handler.trie.Search(strings.ToLower(q))
	if errS != nil {
		return c.NoContent(http.StatusBadRequest)
	}
	return c.JSON(http.StatusOK, values)
}

// postNovel inserts a new episode
func (handler HandlerNovels) postNovel(c echo.Context) error {
	token, err := _echo.GetTokenFromHeader(c)
	if err != nil {
		return utils.BadRequestError(c, err)
	}

	model := models.PostNovelModel{}
	if errMdl := c.Bind(&model); errMdl != nil {
		return utils.BadRequestError(c, errMdl)
	}

	objID, errObj := primitive.ObjectIDFromHex(token.Subscriber)
	if errObj != nil {
		return utils.BadRequestError(c, errObj)
	}
	novelEntity := domain.NovelEntity{
		Owner: domain.UserEntity{
			UserID: objID,
			Name:   token.Name,
		},
		Title:    model.Title,
		Synopsis: model.Synopsis,
		Tags:     utils.SplitAndTrim(model.Tags, ","),
	}
	nvlID, errCrt := handler.NovelRepository.Create(novelEntity)
	if errCrt != nil {
		if exc, ok := errCrt.(mongo.WriteException); ok && strings.Index(exc.WriteErrors.Error(), "duplicate key error collection") > -1 {
			return utils.BadRequestError(c, fmt.Errorf("Duplicated title: %s", novelEntity.Title))
		}
		return utils.InternalServerError(c, errCrt)
	}
	novelResult, errGet := handler.NovelRepository.GetByID(nvlID.Hex())
	if errGet != nil {
		return utils.InternalServerError(c, errGet)
	}

	return c.JSON(http.StatusCreated, handler.entityToGetModel(novelResult))
}

// patchNovel partial update for a specific novel
func (handler HandlerNovels) patchNovel(c echo.Context) error {
	model := models.PatchNovelModel{}
	if err := c.Bind(&model); err != nil {
		return utils.BadRequestError(c, err)
	}

	token, errTkn := _echo.GetTokenFromHeader(c)
	if errTkn != nil {
		return utils.InternalServerError(c, errTkn)
	}

	params := make(map[string]interface{})
	if len(model.Title) > 0 {
		params["Title"] = model.Title
	}
	if len(model.Synopsis) > 0 {
		params["Synopsis"] = model.Synopsis
	}
	tags := utils.SplitAndTrim(model.Tags, ",")
	if len(tags) > 0 {
		params["Synopsis"] = tags
	}
	if model.Status != nil {
		if model.Status == &domain.NovelStatus.Blocked && !token.Role.Check("admin") {
			return c.NoContent(http.StatusUnauthorized)
		}
		params["Status"] = model.Status
	}

	novelID := c.Param("id")
	novel, errGet := handler.NovelRepository.GetByID(novelID)
	if _, ok := errGet.(database.EntityNotFound); ok {
		return c.NoContent(http.StatusNotFound)
	}
	if errGet != nil {
		return utils.InternalServerError(c, errGet)
	}
	if novel.Owner.UserID.Hex() != token.Subscriber {
		return c.NoContent(http.StatusUnauthorized)
	}

	updErr := handler.NovelRepository.Update(novelID, params)
	if _, ok := updErr.(domain.EntityNotModifiedError); ok {
		return c.NoContent(http.StatusNotModified)
	} else if _, ok := updErr.(database.EntityNotFound); ok {
		return c.NoContent(http.StatusNotFound)
	} else if _, ok := updErr.(domain.EntityInvalidStatusError); ok {
		return c.NoContent(http.StatusUnprocessableEntity)
	} else if updErr != nil {
		return utils.InternalServerError(c, updErr)
	}

	return utils.NoContent(c)
}

// deleteNovel deletes an specific novel
func (handler HandlerNovels) deleteNovel(c echo.Context) error {
	novelID := c.Param("id")

	token, errTkn := _echo.GetTokenFromHeader(c)
	if errTkn != nil {
		return utils.InternalServerError(c, errTkn)
	}

	novel, err := handler.NovelRepository.GetByID(novelID)
	if _, ok := err.(database.EntityNotFound); ok {
		return c.NoContent(http.StatusNotFound)
	}
	if err != nil {
		return utils.InternalServerError(c, err)
	}
	if novel.Owner.UserID.Hex() != token.Subscriber && !token.Role.Check("admin") {
		return c.NoContent(http.StatusUnauthorized)
	}

	errDel := handler.NovelRepository.Delete(novelID)
	if errDel != nil {
		return utils.InternalServerError(c, err)
	}

	return utils.NoContent(c)
}

func (handler HandlerNovels) entityToGetModel(entity domain.NovelEntity) models.GetNovelModel {
	return models.GetNovelModel{
		ID:        entity.ID.Hex(),
		Title:     entity.Title,
		Synopsis:  entity.Synopsis,
		Tags:      entity.Tags,
		CreatedAt: entity.CreatedAt.String(),
		Owner: models.GetNovelOwnerModel{
			ID:   entity.Owner.UserID.Hex(),
			Name: entity.Owner.Name,
		},
		Status: entity.Status,
		Cover: models.GetNovelCoverModel{
			Low:    entity.CoverURL.LowQualityFileID,
			Medium: entity.CoverURL.MediumQualityFileID,
			High:   entity.CoverURL.HighQualityFileID,
		},
	}
}
