package handlers

import (
	"errors"
	"fmt"
	"net/http"
	"reflect"
	"strings"

	"github.com/labstack/echo/v4"
	"github.com/marloncristian/oricom-goframework/database"
	_echo "github.com/marloncristian/oricom-goframework/web/echo"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"oricom.io/foundation/api/v1/models"
	"oricom.io/foundation/domain"
	"oricom.io/foundation/domain/repository"
	"oricom.io/foundation/domain/service"
	"oricom.io/foundation/infrastructure"
	"oricom.io/foundation/utils"
	"oricom.io/foundation/utils/assertion"
)

const (
	pristineURL = "https://storage.googleapis.com/evident-torus-273114.appspot.com/pristine.jpg"
)

// HandlerEpisodes handler for episodes api
type HandlerEpisodes struct {
	LogRepository      *repository.Logs             `inject:""`
	NovelRepository    *repository.Novels           `inject:""`
	EpisodeRepository  *repository.Episodes         `inject:""`
	CommmentRepository *repository.EpisodesComments `inject:""`
	Context            *echo.Echo                   `inject:""`
	Mediator           infrastructure.Mediator      `inject:"default.mediator"`
}

// Init the controller configurations
func (handler HandlerEpisodes) Init() {

	handler.Context.GET("/v1/novels/:novelid/episodes", handler.getEpisodes, _echo.Authenticate)
	handler.Context.GET("/v1/novels/:novelid/episodes/:id", handler.getEpisode, _echo.Authenticate)
	handler.Context.GET("/v1/novels/:novelid/episodes/:id/summary", handler.getEpisodeSummary, _echo.Authenticate)

	//restricted
	handler.Context.POST("/v1/novels/:novelid/episodes", handler.postEpisode, _echo.Authenticate, utils.ProducerRequired)
	handler.Context.PATCH("/v1/novels/:novelid/episodes/:id", handler.patchEpisode, _echo.Authenticate, utils.ProducerRequired)
	handler.Context.DELETE("/v1/novels/:novelid/episodes/:id", handler.deleteEpisode, _echo.Authenticate, utils.ProducerRequired)

	//summary
	handler.Context.POST("/v1/novels/:novelid/episodes/:id/summary", handler.getEpisodeSummary, _echo.Authenticate)

	//authors
	handler.Context.POST("/v1/novels/:novelid/episodes/:id/authors", handler.postEpisodeAuthors, _echo.Authenticate, utils.ProducerRequired)
	handler.Context.DELETE("/v1/novels/:novelid/episodes/:id/authors", handler.deleteEpisodeAuthors, _echo.Authenticate, utils.ProducerRequired)

	//contributors
	handler.Context.POST("/v1/novels/:novelid/episodes/:id/contributors", handler.postEpisodeContributor, _echo.Authenticate, utils.ProducerRequired)
	handler.Context.DELETE("/v1/novels/:novelid/episodes/:id/contributors", handler.deleteEpisodeContributor, _echo.Authenticate, utils.ProducerRequired)

	//cover
	handler.Context.GET("/v1/novels/:novelid/episodes/:id/cover", handler.getEpisodeCover, _echo.Authenticate)
	handler.Context.PUT("/v1/novels/:novelid/episodes/:id/cover", handler.putEpisodeCover, _echo.Authenticate, utils.ProducerRequired)

	//comments
	handler.Context.GET("/v1/novels/:novelid/episodes/:episodeid/comments", handler.getEpisodeComments, _echo.Authenticate)
	handler.Context.POST("/v1/novels/:novelid/episodes/:episodeid/comments", handler.postEpisodeComments, _echo.Authenticate)
	handler.Context.PATCH("/v1/novels/:novelid/episodes/:episodeid/comments/:id", handler.patchEpisodeComments, _echo.Authenticate)
	handler.Context.DELETE("/v1/novels/:novelid/episodes/:episodeid/comments/:id", handler.deleteEpisodeComments, _echo.Authenticate)

	//audio
	handler.Context.GET("/v1/novels/:novelid/episodes/:id/audio", handler.getEpisodeAudio, _echo.Authenticate, utils.ProducerRequired)
	handler.Context.PUT("/v1/novels/:novelid/episodes/:id/audio", handler.putEpisodeAudio, _echo.Authenticate, utils.ProducerRequired)
	handler.Context.DELETE("/v1/novels/:novelid/episodes/:id/audio", handler.deleteEpisodeAudio, _echo.Authenticate, utils.ProducerRequired)

	//body
	handler.Context.GET("/v1/novels/:novelid/episodes/:id/body", handler.getEpisodeBody, _echo.Authenticate, utils.ProducerRequired)
	handler.Context.PATCH("/v1/novels/:novelid/episodes/:id/body", handler.patchEpisodeBody, _echo.Authenticate, utils.ProducerRequired)
}

// getEpisode godoc
// @Summary Get a novel episode
// @Description Return a specific novel episode
// @Tags Episodes
// @ID get-episode
// @Accept  json
// @Produce  json
// @Param novelid path int true "Novel ID"
// @Param id path int true "Episode ID"
// @Success 200 {object} models.GetEpisodeModel
// @Header 200 {string} Authorization "Bearer token"
// @Failure 403
// @Failure 400
// @Failure 404
// @Failure 500
// @Router /novels/{novelid}/episodes/{id} [get]
func (handler HandlerEpisodes) getEpisode(c echo.Context) error {
	novelid := c.Param("novelid")
	episodeid := c.Param("id")

	assert := assertion.Assert{}
	failure := assert.Novel().Exists(novelid).Root().Episode().Exists(episodeid).Root().Assert()
	if failure != nil {
		if _, ok := failure.(assertion.NovelDoesNotExistsError); ok {
			return c.NoContent(http.StatusNotFound)
		}
		if _, ok := failure.(assertion.EpisodeDoesNotExistsError); ok {
			return c.NoContent(http.StatusNotFound)
		}
		return utils.InternalServerError(c, failure)
	}

	episode := assert.GetEntity("episode", episodeid).(*domain.NovelEpisodeEntity)

	return c.JSON(http.StatusOK, handler.entityToGetModel(*episode))

	/*
		TODO: implement agregation
		execute de summary update method setting views to views + 1. but this method must be concern
		about invalid access that why we must create a anti corruption layer where the key is ipaddress + time
		to avoid multiple requests in a given period
	*/

}

// getEpisodes godoc
// @Summary Novel episodes query
// @Description Queries for novel episodes
// @Tags Episodes
// @ID get-episodes
// @Accept  json
// @Produce  json
// @Param novelid path int true "Novel ID"
// @Success 200 {object} []models.GetEpisodeModel
// @Header 200 {string} Authorization "Bearer token"
// @Failure 403
// @Failure 400
// @Failure 404
// @Failure 500
// @Router /novels/{novelid}/episodes [get]
func (handler HandlerEpisodes) getEpisodes(c echo.Context) error {
	novelid := c.Param("novelid")

	assert := assertion.Assert{}
	failure := assert.Novel().Exists(novelid).Root().Assert()
	if failure != nil {
		if _, ok := failure.(assertion.NovelDoesNotExistsError); ok {
			return c.NoContent(http.StatusNotFound)
		}
		return utils.InternalServerError(c, failure)
	}

	novel := assert.GetEntity("novel", novelid).(*domain.NovelEntity)

	episodeRep := repository.NewEpisodes()
	episodes, err := episodeRep.GetByNovelID(novel.ID.Hex())
	if err != nil {
		return utils.InternalServerError(c, err)
	}
	if len(episodes) == 0 {
		return utils.NoContent(c)
	}

	result := []models.GetEpisodeModel{}
	for _, episode := range episodes {
		result = append(result, handler.entityToGetModel(episode))
	}

	return c.JSON(http.StatusOK, result)
}

// postEpisode godoc
// @Summary Creates novel episode
// @Description Creates a new novel episode
// @Tags Episodes
// @ID post-episodes
// @Accept  json
// @Produce  json
// @Param episode body models.PostEpisodeModel true "Episode Model"
// @Param novelid path int true "Novel ID"
// @Success 201 {object} []models.GetEpisodeModel
// @Header 201 {string} Authorization "Bearer token"
// @Failure 403
// @Failure 400
// @Failure 404
// @Failure 500
// @Router /novels/{novelid}/episodes [post]
func (handler HandlerEpisodes) postEpisode(c echo.Context) error {

	novelid := c.Param("novelid")

	token, err := _echo.GetTokenFromHeader(c)
	if err != nil {
		return utils.InternalServerError(c, err)
	}

	assert := assertion.Assert{}
	failure := assert.Novel().Exists(novelid).IsOwner(novelid, token.Subscriber).Root().Assert()
	if failure != nil {
		if _, ok := failure.(assertion.NovelDoesNotExistsError); ok {
			return c.NoContent(http.StatusNotFound)
		}
		if _, ok := failure.(assertion.NovelNotOwnerError); ok {
			return c.NoContent(http.StatusForbidden)
		}
		return utils.InternalServerError(c, failure)
	}

	novel := assert.GetEntity("novel", novelid).(*domain.NovelEntity)

	model := models.PostEpisodeModel{}
	if errMdl := c.Bind(&model); errMdl != nil {
		return c.JSON(http.StatusBadRequest, utils.ToEntityViolation(errMdl))
	}

	episodeid := primitive.NewObjectID()
	episode := domain.NovelEpisodeEntity{
		ID:       episodeid,
		Title:    model.Title,
		Synopsis: model.Synopsis,
		NovelID:  novel.ID,
		Cover: &domain.EpisodeCover{
			Redirect: pristineURL,
			URL:      fmt.Sprintf("/novels/%s/episodes/%s/cover", novelid, episodeid.Hex()),
		},
		Summary: &domain.NovelEpisodeSummary{
			Comments: 0,
			Loves:    0,
			Views:    0,
		},
	}

	authorsIDs := utils.SplitAndTrim(model.Authors, ",")

	if len(authorsIDs) > 0 || len(model.Contributors) > 0 {
		usersService := service.Users{}
		users := map[string]domain.UserModel{}
		getUser := func(id string) (domain.UserModel, error) {
			usr, ok := users[id]
			if !ok {
				usrObj, errGetUsr := usersService.GetByID(id)
				if errGetUsr != nil {
					return domain.UserModel{}, errGetUsr
				}
				users[id] = usrObj
				usr = usrObj
			}
			return usr, nil
		}

		if len(authorsIDs) > 0 {
			for _, authorID := range authorsIDs {
				user, errGetUsr := getUser(authorID)
				if errGetUsr != nil {
					return utils.InternalServerError(c, errGetUsr)
				}
				objID, ErrHex := primitive.ObjectIDFromHex(user.ID)
				if ErrHex != nil {
					return utils.InternalServerError(c, ErrHex)
				}
				authorEntity := domain.UserEntity{
					UserID: objID,
					Name:   user.Name,
				}
				episode.Authors = append(episode.Authors, authorEntity)
			}
		}

		if len(model.Contributors) > 0 {
			for _, contributor := range model.Contributors {
				user, errGetUsr := getUser(contributor.ContributorID)
				if errGetUsr != nil {
					return utils.InternalServerError(c, errGetUsr)
				}
				objID, ErrHex := primitive.ObjectIDFromHex(user.ID)
				if ErrHex != nil {
					return utils.InternalServerError(c, ErrHex)
				}
				contributorEntity := domain.ContributorEntity{
					UserID: objID,
					Name:   user.Name,
					Role:   contributor.Role,
				}
				episode.Contributors = append(episode.Contributors, contributorEntity)
			}
		}
	}

	if len(model.Cover) > 0 {
		defer func() {
			go handler.updateCoverFromBase64(episode, model.Cover)
		}()
	}

	rep := repository.NewEpisodes()
	episodeID, errCrt := rep.Create(episode)
	if errCrt != nil {
		if exc, ok := errCrt.(mongo.WriteException); ok && strings.Index(exc.WriteErrors.Error(), "duplicate key error collection") > -1 {
			return utils.BadRequestError(c, fmt.Errorf("Duplicated title: %s", episode.Title))
		}
		return utils.InternalServerError(c, errCrt)
	}

	episodeResult, errGet := rep.GetByID(episodeID.Hex())
	if errGet != nil {
		return utils.InternalServerError(c, errGet)
	}

	return c.JSON(http.StatusCreated, handler.entityToGetModel(episodeResult))
}

// deleteEpisode godoc
// @Summary Deletes a specific novel episode
// @Description Deletes a specific novel episode
// @Tags Episodes
// @ID delete-episodes
// @Accept  json
// @Produce  json
// @Param novelid path int true "Novel ID"
// @Param id path int true "Episode ID"
// @Success 204
// @Header 204 {string} Authorization "Bearer token"
// @Failure 403
// @Failure 400
// @Failure 404
// @Failure 500
// @Router /novels/{novelid}/episodes/{id} [delete]
func (handler HandlerEpisodes) deleteEpisode(c echo.Context) error {

	novelid := c.Param("novelid")
	episodeid := c.Param("id")

	token, err := _echo.GetTokenFromHeader(c)
	if err != nil {
		return utils.InternalServerError(c, err)
	}

	assert := assertion.Assert{}
	failure := assert.Novel().Exists(novelid).IsOwner(novelid, token.Subscriber).Root().Episode().Exists(episodeid).Root().Assert()
	if failure != nil {
		if _, ok := failure.(assertion.NovelDoesNotExistsError); ok {
			return c.NoContent(http.StatusNotFound)
		}
		if _, ok := failure.(assertion.EpisodeDoesNotExistsError); ok {
			return c.NoContent(http.StatusNotFound)
		}
		if _, ok := failure.(assertion.NovelNotOwnerError); ok {
			return c.NoContent(http.StatusForbidden)
		}
		return utils.InternalServerError(c, failure)
	}

	episode := assert.GetEntity("episode", episodeid).(*domain.NovelEpisodeEntity)
	episodeRep := repository.NewEpisodes()
	errDel := episodeRep.Delete(episodeid)
	if errDel != nil {
		if errors.Is(errDel, database.EntityNotFound{}) {
			return c.NoContent(http.StatusNotFound)
		}
		return utils.InternalServerError(c, errDel)
	}

	defer func() {
		go handler.deleteCover(*episode)
	}()

	return utils.NoContent(c)
}

// patchEpisode godoc
// @Summary Partially updates a specific novel episode
// @Description Partially updates a specific novel episode
// @Tags Episodes
// @ID delete-episodes
// @Accept  json
// @Produce  json
// @Param novelid path int true "Novel ID"
// @Param id path int true "Episode ID"
// @Param episode body models.PatchEpisodeModel true "Episode Model"
// @Success 200 {object} []models.GetEpisodeModel
// @Header 200 {string} Authorization "Bearer token"
// @Failure 403
// @Failure 400
// @Failure 404
// @Failure 500
// @Router /novels/{novelid}/episodes/{id} [patch]
func (handler HandlerEpisodes) patchEpisode(c echo.Context) error {

	novelid := c.Param("novelid")
	episodeid := c.Param("id")

	token, err := _echo.GetTokenFromHeader(c)
	if err != nil {
		return utils.InternalServerError(c, err)
	}

	assert := assertion.Assert{}
	failure := assert.Novel().Exists(novelid).IsOwner(novelid, token.Subscriber).Root().Episode().Exists(episodeid).Root().Assert()
	if failure != nil {
		if _, ok := failure.(assertion.NovelDoesNotExistsError); ok {
			return c.NoContent(http.StatusNotFound)
		}
		if _, ok := failure.(assertion.EpisodeDoesNotExistsError); ok {
			return c.NoContent(http.StatusNotFound)
		}
		if _, ok := failure.(assertion.NovelNotOwnerError); ok {
			return c.NoContent(http.StatusForbidden)
		}
		return utils.InternalServerError(c, failure)
	}

	episode := *assert.GetEntity("episode", episodeid).(*domain.NovelEpisodeEntity)

	model := models.PatchEpisodeModel{}
	if errMdl := c.Bind(&model); errMdl != nil {
		return c.JSON(http.StatusBadRequest, utils.ToEntityViolation(errMdl))
	}

	candidate := episode
	if len(model.Title) > 0 {
		candidate.Title = model.Title
	}
	if len(model.Synopsis) > 0 {
		candidate.Synopsis = model.Synopsis
	}

	authorsIDs := utils.SplitAndTrim(model.Authors, ",")
	if len(authorsIDs) > 0 || len(model.Contributors) > 0 {
		usersService := service.Users{}
		users := map[string]domain.UserModel{}
		getUser := func(id string) (domain.UserModel, error) {
			usr, ok := users[id]
			if !ok {
				usrObj, errGetUsr := usersService.GetByID(id)
				if errGetUsr != nil {
					return domain.UserModel{}, errGetUsr
				}
				users[id] = usrObj
				usr = usrObj
			}
			return usr, nil
		}

		if len(authorsIDs) > 0 {
			candidate.Authors = []domain.UserEntity{}
			for _, authorID := range authorsIDs {
				user, errGetUsr := getUser(authorID)
				if errGetUsr != nil {
					return utils.InternalServerError(c, errGetUsr)
				}
				objID, ErrHex := primitive.ObjectIDFromHex(user.ID)
				if ErrHex != nil {
					return utils.InternalServerError(c, ErrHex)
				}
				authorEntity := domain.UserEntity{
					UserID: objID,
					Name:   user.Name,
				}
				candidate.Authors = append(candidate.Authors, authorEntity)
			}
		}

		if len(model.Contributors) > 0 {
			candidate.Contributors = []domain.ContributorEntity{}
			for _, contributor := range model.Contributors {
				user, errGetUsr := getUser(contributor.ContributorID)
				if errGetUsr != nil {
					return utils.InternalServerError(c, errGetUsr)
				}
				objID, ErrHex := primitive.ObjectIDFromHex(user.ID)
				if ErrHex != nil {
					return utils.InternalServerError(c, ErrHex)
				}
				contributorEntity := domain.ContributorEntity{
					UserID: objID,
					Name:   user.Name,
					Role:   contributor.Role,
				}
				candidate.Contributors = append(candidate.Contributors, contributorEntity)
			}
		}
	}
	coverUpdated := false
	if len(model.Cover) > 0 {
		hashMD5, err := utils.GetMD5FromBase64(model.Cover)
		if err != nil {
			return utils.InternalServerError(c, err)
		}
		coverUpdated = episode.Cover == nil || episode.Cover.Hash != hashMD5
		if coverUpdated {
			defer func() {
				go handler.updateCoverFromBase64(episode, model.Cover)
			}()
		}
	}

	if reflect.DeepEqual(episode, candidate) && !coverUpdated {
		return c.NoContent(http.StatusNotModified)
	}

	rep := repository.NewEpisodes()
	replaceErr := rep.Replace(candidate)
	if replaceErr != nil {
		if exc, ok := replaceErr.(mongo.WriteException); ok && strings.Index(exc.WriteErrors.Error(), "duplicate key error collection") > -1 {
			return utils.BadRequestError(c, fmt.Errorf("Duplicated title: %s", candidate.Title))
		}
		return utils.InternalServerError(c, replaceErr)
	}

	episodeResult, errGet := rep.GetByID(episode.ID.Hex())
	if errGet != nil {
		return utils.InternalServerError(c, errGet)
	}

	return c.JSON(http.StatusOK, handler.entityToGetModel(episodeResult))
}

func (handler HandlerEpisodes) entityToGetModel(entity domain.NovelEpisodeEntity) models.GetEpisodeModel {
	model := models.GetEpisodeModel{
		ID:        entity.ID.Hex(),
		Title:     entity.Title,
		Synopsis:  entity.Synopsis,
		Status:    entity.Status,
		CreatedAt: entity.CreatedAt.String(),
	}
	if entity.Audio != nil {
		model.Audio = &models.GetEpisodeAudioModel{
			URL: entity.Audio.URL,
		}
	}
	if entity.Cover != nil {
		model.Cover = &models.GetEpisodeCoverModel{
			URL: entity.Cover.URL,
		}
	}
	authors := []models.GetEpisodeAuthorModel{}
	for _, author := range entity.Authors {
		authors = append(authors, models.GetEpisodeAuthorModel{
			AuthorID: author.UserID.Hex(),
			Name:     author.Name,
		})
	}
	model.Authors = authors
	contributors := []models.GetEpisodeContributorsModel{}
	for _, contributor := range entity.Contributors {
		contributors = append(contributors, models.GetEpisodeContributorsModel{
			ContributorID: contributor.UserID.Hex(),
			Name:          contributor.Name,
			Role:          contributor.Role,
		})
	}
	model.Contributors = contributors
	return model
}
