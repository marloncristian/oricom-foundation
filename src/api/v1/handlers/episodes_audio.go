package handlers

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"net/http"

	"github.com/labstack/echo/v4"
	_echo "github.com/marloncristian/oricom-goframework/web/echo"
	"oricom.io/foundation/domain"
	"oricom.io/foundation/domain/repository"
	"oricom.io/foundation/domain/service"
	"oricom.io/foundation/utils"
	"oricom.io/foundation/utils/assertion"
)

func (handler HandlerEpisodes) getEpisodeAudio(c echo.Context) error {

	novelid := c.Param("novelid")
	episodeid := c.Param("id")

	assert := assertion.Assert{}
	failure := assert.Novel().Exists(novelid).Root().Episode().Exists(episodeid).Root().Assert()
	if failure != nil {
		if _, ok := failure.(assertion.NovelDoesNotExistsError); ok {
			return c.NoContent(http.StatusNotFound)
		}
		if _, ok := failure.(assertion.EpisodeDoesNotExistsError); ok {
			return c.NoContent(http.StatusNotFound)
		}
		return utils.InternalServerError(c, failure)
	}

	episode := assert.GetEntity("episode", episodeid).(*domain.NovelEpisodeEntity)
	if episode.Audio == nil {
		return utils.NoContent(c)
	}

	/* TODO: quality switch
	quality := c.QueryParam("quality")
	if len(quality) == 0 {
		quality = "medium"
	} else if quality != "medium" && quality != "low" && quality != "high" {
		return c.NoContent(http.StatusNotFound)
	}
	*/

	c.Response().Header().Set("Location", episode.Audio.Redirect)
	return c.NoContent(302)
}

func (handler HandlerEpisodes) putEpisodeAudio(c echo.Context) error {
	uploadType := c.QueryParam("uploadtype")
	if uploadType != "multipart" {
		return utils.BadRequestError(c, errors.New("Only updload method accepted is multipart for now"))
	}

	novelid := c.Param("novelid")
	episodeid := c.Param("id")

	token, err := _echo.GetTokenFromHeader(c)
	if err != nil {
		return utils.InternalServerError(c, err)
	}

	assert := assertion.Assert{}
	failure := assert.Novel().Exists(novelid).IsOwner(novelid, token.Subscriber).Root().Episode().Exists(episodeid).Root().Assert()
	if failure != nil {
		if _, ok := failure.(assertion.NovelDoesNotExistsError); ok {
			return c.NoContent(http.StatusNotFound)
		}
		if _, ok := failure.(assertion.EpisodeDoesNotExistsError); ok {
			return c.NoContent(http.StatusNotFound)
		}
		return utils.InternalServerError(c, failure)
	}

	file, err := c.FormFile("file")
	if err != nil {
		return utils.InternalServerError(c, err)
	}

	contentType := file.Header.Get("Content-Type")
	if contentType != "audio/mpeg" {
		return utils.BadRequestError(c, errors.New("Invalid file type"))
	}

	src, err := file.Open()
	if err != nil {
		return err
	}
	defer src.Close()

	buf := bytes.NewBuffer(nil)
	if _, err := io.Copy(buf, src); err != nil {
		return utils.InternalServerError(c, err)
	}
	content := buf.Bytes()

	episode := *assert.GetEntity("episode", episodeid).(*domain.NovelEpisodeEntity)

	hashMD5, err := utils.GetMD5FromByteArray(content)
	if err != nil {
		return utils.InternalServerError(c, err)
	}

	if episode.Audio != nil && hashMD5 == episode.Audio.Hash {
		return c.NoContent(http.StatusNotModified)
	}

	go handler.updateAudio(episode, content)

	return utils.NoContent(c)
}

func (handler HandlerEpisodes) deleteEpisodeAudio(c echo.Context) error {

	novelid := c.Param("novelid")
	episodeid := c.Param("id")

	token, err := _echo.GetTokenFromHeader(c)
	if err != nil {
		return utils.InternalServerError(c, err)
	}

	assert := assertion.Assert{}
	failure := assert.Novel().Exists(novelid).IsOwner(novelid, token.Subscriber).Root().Episode().Exists(episodeid).Root().Assert()
	if failure != nil {
		if _, ok := failure.(assertion.NovelDoesNotExistsError); ok {
			return c.NoContent(http.StatusNotFound)
		}
		if _, ok := failure.(assertion.EpisodeDoesNotExistsError); ok {
			return c.NoContent(http.StatusNotFound)
		}
		return utils.InternalServerError(c, failure)
	}

	episode := *assert.GetEntity("episode", episodeid).(*domain.NovelEpisodeEntity)
	if episode.Audio == nil {
		return c.NoContent(http.StatusNotModified)
	}

	updateMap := make(map[string]interface{})
	updateMap["audio"] = nil

	rep := repository.NewEpisodes()
	updateErr := rep.Update(episodeid, updateMap)
	if updateErr != nil {
		return utils.InternalServerError(c, updateErr)
	}

	go handler.deleteAudio(episode)

	return utils.NoContent(c)
}

func (handler *HandlerEpisodes) updateAudio(episode domain.NovelEpisodeEntity, content []byte) {
	/*
		TODO: rebuild
		temporarily and due to a lack of suitable infrastructure, the process of sending the file is
		done asynchronously but still very precarious. In the future, this process needs to be
		transformed into a retained queue structure.
	*/

	if len(content) == 0 {
		handler.LogRepository.Error("Empty content parameter")
		return
	}

	hashMD5, err := utils.GetMD5FromByteArray(content)
	if err != nil {
		handler.LogRepository.Error(err.Error())
		return
	}

	if episode.Audio != nil && hashMD5 == episode.Audio.Hash {
		handler.LogRepository.Error("No modification")
		return
	}

	storageService := service.Storage{}
	filename := fmt.Sprintf("%s-%s", episode.ID.Hex(), hashMD5)

	link, err := storageService.UploadBytes(content, fmt.Sprintf("%s/audios/%s", episode.ID.Hex(), filename))
	if err != nil {
		handler.LogRepository.Error(err.Error())
		return
	}

	updateMap := make(map[string]interface{})
	updateMap["audio"] = domain.EpisodeCover{
		Hash:     hashMD5,
		URL:      fmt.Sprintf("/novels/%s/episodes/%s/audio", episode.NovelID.Hex(), episode.ID.Hex()),
		Redirect: link,
	}

	err = handler.EpisodeRepository.Update(episode.ID.Hex(), updateMap)
	if err != nil {
		handler.LogRepository.Error(err.Error())
	}

	if episode.Audio != nil {
		handler.deleteAudio(episode)
	}
}

func (handler *HandlerEpisodes) deleteAudio(episode domain.NovelEpisodeEntity) {
	/*
		TODO: rebuild
		temporarily and due to a lack of suitable infrastructure, the process of sending the file is
		done asynchronously but still very precarious. In the future, this process needs to be
		transformed into a retained queue structure.
	*/

	if episode.Audio == nil {
		return
	}

	storageService := service.Storage{}
	filename := fmt.Sprintf("%s-%s", episode.ID.Hex(), episode.Audio.Hash)
	err := storageService.DeleteFile(fmt.Sprintf("%s/audios/%s", episode.ID.Hex(), filename))
	if err != nil {
		handler.LogRepository.Error(err.Error())
		return
	}
}
