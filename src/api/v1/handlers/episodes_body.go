package handlers

import (
	"net/http"

	"github.com/labstack/echo/v4"
	_echo "github.com/marloncristian/oricom-goframework/web/echo"
	"oricom.io/foundation/api/v1/models"
	"oricom.io/foundation/domain"
	"oricom.io/foundation/domain/repository"
	"oricom.io/foundation/utils"
	"oricom.io/foundation/utils/assertion"
)

//body
func (handler HandlerEpisodes) getEpisodeBody(c echo.Context) error {

	novelid := c.Param("novelid")
	episodeid := c.Param("id")

	assert := assertion.Assert{}
	failure := assert.Novel().Exists(novelid).Root().Episode().Exists(episodeid).Root().Assert()
	if failure != nil {
		if _, ok := failure.(assertion.NovelDoesNotExistsError); ok {
			return c.NoContent(http.StatusNotFound)
		}
		if _, ok := failure.(assertion.EpisodeDoesNotExistsError); ok {
			return c.NoContent(http.StatusNotFound)
		}
		return utils.InternalServerError(c, failure)
	}

	episode := *assert.GetEntity("episode", episodeid).(*domain.NovelEpisodeEntity)
	if episode.Body == nil {
		return utils.NoContent(c)
	}

	return c.JSON(http.StatusBadRequest, models.GetEpisodeBody{
		Body: episode.Body.Data,
	})
}

func (handler HandlerEpisodes) patchEpisodeBody(c echo.Context) error {

	model := models.PatchEpisodeBody{}
	if errMdl := c.Bind(&model); errMdl != nil {
		return c.JSON(http.StatusBadRequest, utils.ToEntityViolation(errMdl))
	}

	token, err := _echo.GetTokenFromHeader(c)
	if err != nil {
		return utils.InternalServerError(c, err)
	}

	novelid := c.Param("novelid")
	episodeid := c.Param("id")

	assert := assertion.Assert{}
	failure := assert.Novel().Exists(novelid).IsOwner(novelid, token.Subscriber).Root().Episode().Exists(episodeid).Root().Assert()
	if failure != nil {
		if _, ok := failure.(assertion.NovelDoesNotExistsError); ok {
			return c.NoContent(http.StatusNotFound)
		}
		if _, ok := failure.(assertion.EpisodeDoesNotExistsError); ok {
			return c.NoContent(http.StatusNotFound)
		}
		return utils.InternalServerError(c, failure)
	}

	episode := *assert.GetEntity("episode", episodeid).(*domain.NovelEpisodeEntity)

	hashMD5, err := utils.GetMD5FromString(model.Body)
	if err != nil {
		return utils.InternalServerError(c, err)
	}

	if episode.Body != nil && hashMD5 != episode.Body.Hash {
		return c.NoContent(http.StatusNotModified)
	}

	updateMap := make(map[string]interface{})
	updateMap["body"] = domain.EpisodeBody{
		Data: model.Body,
		Hash: hashMD5,
	}

	rep := repository.NewEpisodes()
	updateErr := rep.Update(episodeid, updateMap)
	if updateErr != nil {
		return utils.InternalServerError(c, updateErr)
	}

	return utils.NoContent(c)
}
