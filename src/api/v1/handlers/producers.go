package handlers

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
	"github.com/marloncristian/oricom-goframework/database"
	_echo "github.com/marloncristian/oricom-goframework/web/echo"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"oricom.io/foundation/api/v1/models"
	"oricom.io/foundation/domain"
	"oricom.io/foundation/domain/repository"
	"oricom.io/foundation/utils"
)

// HandlerProducers handler for users api
type HandlerProducers struct {
	ProducerRepository *repository.Producers `inject:""`
	Context            *echo.Echo            `inject:""`
}

// Init the controller configurations
func (handler HandlerProducers) Init() {

	//admin only routes
	handler.Context.GET("/v1/producers", handler.getProducers, _echo.AuthenticateAdmin)
	handler.Context.PATCH("/v1/producers/:id", handler.patchProducer, _echo.AuthenticateAdmin)

	//default routes
	handler.Context.GET("/v1/producers/:id", handler.getProducer, _echo.Authenticate)

	//me route
	handler.Context.GET("/v1/me/producers", handler.getMeProducer, _echo.Authenticate)
	handler.Context.PATCH("/v1/me/producers", handler.patchMeProducer, _echo.Authenticate)
	handler.Context.POST("/v1/me/producers", handler.postMeProducer, _echo.Authenticate)
}

// getProducers returns all producers records
func (handler HandlerProducers) getProducers(c echo.Context) error {

	offset := int64(0)
	if offsetParam := c.QueryParam("_offset"); len(offsetParam) > 0 {
		offsetPar, err := strconv.ParseInt(offsetParam, 10, 64)
		if err == nil {
			offset = offsetPar
		}
	}

	limit := int64(100)
	if limitParam := c.QueryParam("_limit"); len(limitParam) > 0 {
		limitPar, err := strconv.ParseInt(limitParam, 10, 64)
		if err == nil {
			limit = limitPar
		}
	}

	query := bson.M{}
	if displayname := c.QueryParam("displayname"); len(displayname) > 0 {
		query = bson.M{"displayname": primitive.Regex{Pattern: displayname, Options: "i"}}
	}

	producers, err := handler.ProducerRepository.GetWithSkipLimit(query, offset, limit)
	if err != nil {
		return utils.UnauthorizedError(c, err)
	}
	if len(producers) == 0 {
		return utils.NoContent(c)
	}

	response := []models.GetProducerModel{}
	for _, item := range producers {
		response = append(response, models.GetProducerModel{
			ID:          item.ID.Hex(),
			DisplayName: item.DisplayName,
			Status:      item.Status,
			CreatedAt:   item.CreatedAt,
			ActivatedAt: item.ActivatedAt,
		})
	}

	c.Response().Header().Set("Content-Range", fmt.Sprintf("%c-%c", offset, offset+limit))
	c.Response().Header().Set("Accept-Range", string(limit))
	return c.JSON(http.StatusOK, response)
}

// getProducer returns a specific producer by id
func (handler HandlerProducers) getProducer(c echo.Context) error {
	prcID := c.Param("id")

	prc, err := handler.ProducerRepository.GetByID(prcID)
	if _, ok := err.(database.EntityNotFound); ok {
		return c.NoContent(http.StatusNotFound)
	}
	if err != nil {
		return utils.InternalServerError(c, err)
	}

	return c.JSON(http.StatusOK, models.GetProducerModel{
		ID:          prc.ID.Hex(),
		DisplayName: prc.DisplayName,
		Status:      prc.Status,
		CreatedAt:   prc.CreatedAt,
		ActivatedAt: prc.ActivatedAt,
	})
}

// getMeProducer returns a specific producer for current context user
func (handler HandlerProducers) getMeProducer(c echo.Context) error {
	token, errTkn := _echo.GetTokenFromHeader(c)
	if errTkn != nil {
		return utils.BadRequestError(c, errTkn)
	}

	prc, err := handler.ProducerRepository.GetByID(token.Subscriber)
	if _, ok := err.(database.EntityNotFound); ok {
		return c.NoContent(http.StatusNotFound)
	}
	if err != nil {
		return utils.InternalServerError(c, err)
	}

	return c.JSON(http.StatusOK, models.GetProducerModel{
		ID:          prc.ID.Hex(),
		DisplayName: prc.DisplayName,
		Status:      prc.Status,
		CreatedAt:   prc.CreatedAt,
		ActivatedAt: prc.ActivatedAt,
	})
}

// postProducer sets the requisition for producer in the pipeline
func (handler HandlerProducers) postMeProducer(c echo.Context) error {
	token, errTkn := _echo.GetTokenFromHeader(c)
	if errTkn != nil {
		return utils.BadRequestError(c, errTkn)
	}

	objID, errObj := primitive.ObjectIDFromHex(token.Subscriber)
	if errObj != nil {
		return utils.BadRequestError(c, errObj)
	}

	model := models.PostProducerModel{}
	if errBnd := c.Bind(&model); errBnd != nil {
		if errBnd.Error() != "EOF" {
			return utils.BadRequestError(c, errBnd)
		}
	}

	if len(model.DisplayName) == 0 {
		if len(token.Name) == 0 {
			model.DisplayName = token.Subscriber
		} else {
			model.DisplayName = token.Name
		}
	}

	producer := domain.ProducerEntity{
		ID:          objID,
		DisplayName: model.DisplayName,
		Status:      domain.ProducerStatus.Activating,
	}
	if _, err := handler.ProducerRepository.Create(producer); err != nil {
		return utils.BadRequestError(c, err)
	}

	prc, err := handler.ProducerRepository.GetByID(token.Subscriber)
	if _, ok := err.(database.EntityNotFound); ok {
		return c.NoContent(http.StatusNotFound)
	}
	if err != nil {
		return utils.BadRequestError(c, err)
	}

	return c.JSON(http.StatusCreated, models.GetProducerModel{
		ID:          prc.ID.Hex(),
		DisplayName: prc.DisplayName,
		Status:      prc.Status,
		CreatedAt:   prc.CreatedAt,
		ActivatedAt: prc.ActivatedAt,
	})
}

// patchProducer updates the producer displayname
func (handler HandlerProducers) patchProducer(c echo.Context) error {
	model := models.PatchProducerModel{}
	if err := c.Bind(&model); err != nil {
		return utils.BadRequestError(c, err)
	}

	params := make(map[string]interface{})
	if len(model.DisplayName) > 0 {
		params["DisplayName"] = model.DisplayName
	}
	if model.Status != nil {
		params["Status"] = model.Status
	}

	prcID := c.Param("id")

	updErr := handler.ProducerRepository.Update(prcID, params)
	if _, ok := updErr.(domain.EntityNotModifiedError); ok {
		return c.NoContent(http.StatusNotModified)
	}
	if _, ok := updErr.(database.EntityNotFound); ok {
		return c.NoContent(http.StatusNotFound)
	}
	if _, ok := updErr.(domain.EntityInvalidStatusError); ok {
		return c.NoContent(http.StatusUnprocessableEntity)
	}
	if updErr != nil {
		return utils.InternalServerError(c, updErr)
	}

	return c.NoContent(http.StatusOK)
}

// patchProducer updates the producer displayname
func (handler HandlerProducers) patchMeProducer(c echo.Context) error {
	token, tokErr := _echo.GetTokenFromHeader(c)
	if tokErr != nil {
		return utils.BadRequestError(c, tokErr)
	}

	model := models.PatchProducerModel{}
	if decErr := c.Bind(&model); decErr != nil {
		return utils.BadRequestError(c, decErr)
	}

	updErr := handler.ProducerRepository.Update(token.Subscriber, map[string]interface{}{
		"DisplayName": model.DisplayName,
	})
	if _, ok := updErr.(database.EntityNotFound); ok {
		return c.NoContent(http.StatusNotFound)
	}
	if _, ok := updErr.(domain.EntityNotModifiedError); ok {
		return c.NoContent(http.StatusNotModified)
	}
	if _, ok := updErr.(domain.EntityInvalidStatusError); ok {
		return c.NoContent(http.StatusUnprocessableEntity)
	}
	if updErr != nil {
		return utils.InternalServerError(c, updErr)
	}

	return c.NoContent(http.StatusOK)
}
