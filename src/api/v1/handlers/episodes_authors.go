package handlers

import (
	"net/http"

	"github.com/labstack/echo/v4"
	_echo "github.com/marloncristian/oricom-goframework/web/echo"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"oricom.io/foundation/api/v1/models"
	"oricom.io/foundation/domain"
	"oricom.io/foundation/domain/service"
	"oricom.io/foundation/utils"
	"oricom.io/foundation/utils/assertion"
)

func (handler HandlerEpisodes) postEpisodeAuthors(c echo.Context) error {

	model := models.PostEpisodeAuthorModel{}
	if errMdl := c.Bind(&model); errMdl != nil {
		return c.JSON(http.StatusBadRequest, utils.ToEntityViolation(errMdl))
	}

	novelid := c.Param("novelid")
	episodeid := c.Param("id")

	token, err := _echo.GetTokenFromHeader(c)
	if err != nil {
		return utils.InternalServerError(c, err)
	}

	assert := assertion.Assert{}
	failure := assert.Novel().Exists(novelid).IsOwner(novelid, token.Subscriber).Root().Episode().Exists(episodeid).Root().Assert()
	if failure != nil {
		if _, ok := failure.(assertion.NovelDoesNotExistsError); ok {
			return c.NoContent(http.StatusNotFound)
		}
		if _, ok := failure.(assertion.EpisodeDoesNotExistsError); ok {
			return c.NoContent(http.StatusNotFound)
		}
		if _, ok := failure.(assertion.NovelNotOwnerError); ok {
			return c.NoContent(http.StatusForbidden)
		}
		return utils.InternalServerError(c, failure)
	}

	episode := *assert.GetEntity("episode", episodeid).(*domain.NovelEpisodeEntity)

	usersService := service.Users{}
	authors := episode.Authors
	modified := false
	for _, candidate := range model.AuthorsID {
		found := false
		for _, author := range authors {
			if author.UserID.Hex() == candidate {
				found = true
				break
			}
		}
		if !found {
			user, err := usersService.GetByID(candidate)
			if err != nil {
				return c.NoContent(http.StatusInternalServerError)
			}
			userID, err := primitive.ObjectIDFromHex(user.ID)
			if err != nil {
				return c.NoContent(http.StatusInternalServerError)
			}
			authors = append(authors, domain.UserEntity{
				UserID: userID,
				Name:   user.Name,
			})
			modified = true
		}
	}
	if !modified {
		return c.NoContent(http.StatusNotModified)
	}

	updateMap := make(map[string]interface{})
	updateMap["authors"] = authors
	updateErr := handler.EpisodeRepository.Update(episodeid, updateMap)
	if updateErr != nil {
		return c.NoContent(http.StatusInternalServerError)
	}

	return c.NoContent(http.StatusCreated)
}

func (handler HandlerEpisodes) deleteEpisodeAuthors(c echo.Context) error {

	model := models.DeleteEpisodeAuthorModel{}
	if errMdl := c.Bind(&model); errMdl != nil {
		return c.JSON(http.StatusBadRequest, utils.ToEntityViolation(errMdl))
	}

	novelid := c.Param("novelid")
	episodeid := c.Param("id")

	token, err := _echo.GetTokenFromHeader(c)
	if err != nil {
		return utils.InternalServerError(c, err)
	}

	assert := assertion.Assert{}
	failure := assert.Novel().Exists(novelid).IsOwner(novelid, token.Subscriber).Root().Episode().Exists(episodeid).Root().Assert()
	if failure != nil {
		if _, ok := failure.(assertion.NovelDoesNotExistsError); ok {
			return c.NoContent(http.StatusNotFound)
		}
		if _, ok := failure.(assertion.EpisodeDoesNotExistsError); ok {
			return c.NoContent(http.StatusNotFound)
		}
		if _, ok := failure.(assertion.NovelNotOwnerError); ok {
			return c.NoContent(http.StatusForbidden)
		}
		return utils.InternalServerError(c, failure)
	}

	episode := *assert.GetEntity("episode", episodeid).(*domain.NovelEpisodeEntity)

	authors := []domain.UserEntity{}
	for _, author := range episode.Authors {
		found := false
		for _, candidate := range model.AuthorsID {
			if author.UserID.Hex() == candidate {
				found = true
				break
			}
		}
		if !found {
			authors = append(authors, author)
		}
	}
	if len(authors) != len(episode.Authors) {
		return c.NoContent(http.StatusNotModified)
	}

	updateMap := make(map[string]interface{})
	updateMap["authors"] = authors
	updateErr := handler.EpisodeRepository.Update(episodeid, updateMap)
	if updateErr != nil {
		return c.NoContent(http.StatusInternalServerError)
	}

	return utils.NoContent(c)
}
