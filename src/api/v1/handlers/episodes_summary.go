package handlers

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"oricom.io/foundation/domain"
	"oricom.io/foundation/utils"
	"oricom.io/foundation/utils/assertion"
)

// getEpisodeSummary godoc
// @Summary Request novel episode summary
// @Description Requests novel episode summary
// @Tags Episodes
// @ID get-episode-summary
// @Accept  json
// @Produce  json
// @Param novelid path int true "Novel ID"
// @Param id path int true "Episode ID"
// @Param episode body models.PatchEpisodeModel true "Episode Model"
// @Success 200 {object} []models.GetEpisodeModel
// @Header 200 {string} Authorization "Bearer token"
// @Failure 403
// @Failure 400
// @Failure 404
// @Failure 500
// @Router /novels/{novelid}/episodes/{id}/summary [get]
// getEpisodeSummary returns the episode summary
func (handler HandlerEpisodes) getEpisodeSummary(c echo.Context) error {

	novelid := c.Param("novelid")
	episodeid := c.Param("id")

	assert := assertion.Assert{}
	failure := assert.Novel().Exists(novelid).Root().Episode().Exists(episodeid).Root().Assert()
	if failure != nil {
		if _, ok := failure.(assertion.NovelDoesNotExistsError); ok {
			return c.NoContent(http.StatusNotFound)
		}
		if _, ok := failure.(assertion.EpisodeDoesNotExistsError); ok {
			return c.NoContent(http.StatusNotFound)
		}
		return utils.InternalServerError(c, failure)
	}

	episode := assert.GetEntity("episode", episodeid).(*domain.NovelEpisodeEntity)

	if episode.Summary == nil {
		return utils.NoContent(c)
	}

	return c.JSON(http.StatusOK, episode.Summary)
}
