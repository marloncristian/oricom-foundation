package models

// PostNovelModel model for novel inserting
type PostNovelModel struct {
	Title    string `json:"title" binding:"required,min=3,max=1024"`
	Synopsis string `json:"synopsis" binding:"max=5120"`
	Tags     string `json:"tags"`
}

// GetNovelModel model for requesting novel
type GetNovelModel struct {
	ID        string             `json:"id"`
	Title     string             `json:"title"`
	Synopsis  string             `json:"synopsis,omitempty"`
	Tags      []string           `json:"tags,omitempty"`
	Status    int32              `json:"status"`
	CreatedAt string             `json:"createdat"`
	Owner     GetNovelOwnerModel `json:"owner"`
	Cover     GetNovelCoverModel `json:"cover,omitempty"`
}

// GetNovelOwnerModel user entity owner model
type GetNovelOwnerModel struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

// GetNovelCoverModel model for novel cover
type GetNovelCoverModel struct {
	Low    string `json:"low,omitempty"`
	Medium string `json:"medium,omitempty"`
	High   string `json:"high,omitempty"`
}

// PatchNovelModel model for novel updating
type PatchNovelModel struct {
	Title    string `json:"title"`
	Synopsis string `json:"synopsis,omitempty"`
	Tags     string `json:"tags,omitempty"`
	Status   *int32 `json:"status"`
}
