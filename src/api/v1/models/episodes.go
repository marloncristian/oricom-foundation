package models

import "time"

//PostEpisodeModel model for episode posting
type PostEpisodeModel struct {
	Title        string                        `json:"title" binding:"required,min=3,max=1024"`
	Synopsis     string                        `json:"synopsis" binding:"max=5120"`
	Cover        string                        `json:"cover"`
	Authors      string                        `json:"authors"`
	Contributors []PostEpisodeContributorModel `json:"contributors"`
}

// PostEpisodeContributorsModel model for episode contrubutors posting
type PostEpisodeContributorsModel struct {
	Contributors []PostEpisodeContributorModel `json:"contributors" binding:"required"`
}

// PostEpisodeContributorModel single contributor get model
type PostEpisodeContributorModel struct {
	ContributorID string `json:"id" binding:"required"`
	Role          string `json:"role" binding:"required"`
}

// DeleteEpisodeContributorModel delete episode contributor
type DeleteEpisodeContributorModel struct {
	ContributorsID []string `json:"contributors" binding:"required"`
}

//PatchEpisodeModel model for episode posting
type PatchEpisodeModel struct {
	Title        string                        `json:"title" binding:"required,min=3,max=1024"`
	Synopsis     string                        `json:"synopsis" binding:"max=5120"`
	Cover        string                        `json:"cover"`
	Authors      string                        `json:"authors"`
	Contributors []PostEpisodeContributorModel `json:"contributors"`
}

// PatchEpisodeContributorsModel model for episode contrubutors posting
type PatchEpisodeContributorsModel struct {
	ContributorID string `json:"id"`
	Role          string `json:"role"`
}

// GetEpisodeModel model for episode requesting
type GetEpisodeModel struct {
	ID           string                        `json:"id"`
	Title        string                        `json:"title"`
	Synopsis     string                        `json:"synopsis,omitempty"`
	Status       int32                         `json:"status"`
	Audio        *GetEpisodeAudioModel         `json:"audio,omitempty"`
	Authors      []GetEpisodeAuthorModel       `json:"authors,omitempty"`
	Contributors []GetEpisodeContributorsModel `json:"contributors,omitempty"`
	CreatedAt    string                        `json:"createdat"`
	Cover        *GetEpisodeCoverModel         `json:"cover,omitempty"`
}

// GetEpisodeAuthorModel model for episode author
type GetEpisodeAuthorModel struct {
	AuthorID string `json:"id"`
	Name     string `json:"name"`
}

// PostEpisodeAuthorModel model for episode author
type PostEpisodeAuthorModel struct {
	AuthorsID []string `json:"authors" validate:"required"`
}

// DeleteEpisodeAuthorModel model for episode author
type DeleteEpisodeAuthorModel struct {
	AuthorsID []string `json:"authors" validate:"required"`
}

// GetEpisodeContributorsModel model for episode contributors posting
type GetEpisodeContributorsModel struct {
	ContributorID string `json:"id"`
	Name          string `json:"name"`
	Role          string `json:"role"`
}

// GetEpisodeCoverModel model for episode cover
type GetEpisodeCoverModel struct {
	URL string `json:"url,omitempty"`
}

// GetEpisodeAudioModel model for episode cover
type GetEpisodeAudioModel struct {
	URL string `json:"url,omitempty"`
}

// GetEpisodeBody model for requesting episode body
type GetEpisodeBody struct {
	Body string `json:"body"`
}

// PatchEpisodeBody model for updating episode body
type PatchEpisodeBody struct {
	Body string `json:"body"`
}

// GetEpisodesCommentsModel model for requesting episodes comments
type GetEpisodesCommentsModel struct {
	ID        string                       `json:"id"`
	EpisodeID string                       `json:"episodeid"`
	User      GetEpisodesCommentsUserModel `json:"user"`
	Comment   string                       `json:"commend"`
	CreatedAt time.Time                    `json:"createdat"`
	UpdatedAt time.Time                    `json:"updatedat"`
}

// GetEpisodesCommentsUserModel model for requesting user information from comments
type GetEpisodesCommentsUserModel struct {
	UserID string `json:"id"`
	Name   string `json:"name"`
}

// PostEpisodesCommentsModel post episode comment
type PostEpisodesCommentsModel struct {
	Comment string
}
