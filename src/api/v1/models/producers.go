package models

import (
	"time"
)

// GetProducerModel response model for producer query
type GetProducerModel struct {
	ID          string     `json:"id"`
	Status      int32      `json:"status"`
	DisplayName string     `json:"displayname"`
	CreatedAt   time.Time  `json:"createdat"`
	ActivatedAt *time.Time `json:"activatedat,omitempty"`
}

// PostProducerModel model for producer posting
type PostProducerModel struct {
	DisplayName string `json:"displayname"`
}

// PatchProducerModel model for producer updating
type PatchProducerModel struct {
	DisplayName string `json:"displayname"`
	Status      *int32 `json:"status"`
}
