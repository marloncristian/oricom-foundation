package api

import (
	"oricom.io/foundation/api/v1/handlers"
	"oricom.io/foundation/utils"
)

// Server has router and db instances
type Server struct {
	Configuration    *utils.Configuration       `inject:""`
	HandlerProducers *handlers.HandlerProducers `inject:""`
	HandlerEpisodes  *handlers.HandlerEpisodes  `inject:""`
	HandlerNovels    *handlers.HandlerNovels    `inject:""`
}

// Run initializes the app with predefined configuration
func (a *Server) Run() {
	a.HandlerEpisodes.Init()
	a.HandlerNovels.Init()
	a.HandlerProducers.Init()
}

// NewServer creates a new instance of server
func NewServer(configParameter *utils.Configuration) *Server {
	return &Server{
		Configuration: configParameter,
	}
}
