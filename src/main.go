package main

import (
	"fmt"
	"net/http"
	"os"

	"oricom.io/foundation/api"
	"oricom.io/foundation/domain/events"
	"oricom.io/foundation/domain/events/handlers"
	"oricom.io/foundation/domain/repository"
	"oricom.io/foundation/infrastructure"
	"oricom.io/foundation/utils"

	"github.com/facebookgo/inject"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/marloncristian/oricom-goframework/database"
	"github.com/marloncristian/oricom-goframework/web/authentication"

	echoSwagger "github.com/swaggo/echo-swagger"
	_ "oricom.io/foundation/docs"
)

// @title Oricom Foundation API
// @version 1.0
// @description Foundation API for Oricom

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @host oricom-foundation.herokuapp.com
// @BasePath /v1
func main() {

	//initializes the bases for the aplication to start
	database.Initialize(utils.ConfigurationInstance.GetServerURL(), utils.ConfigurationInstance.DatabaseName)
	authentication.Initialize(utils.ConfigurationInstance.Security.Secret)

	fmt.Fprintln(os.Stdout, "initializing...")

	mediator := infrastructure.MediatorImpl{}
	mediator.Handle(events.CommentCreatedMessage{}, handlers.CommentCreatedHandler)

	//application infrastructure
	server := api.Server{}
	context := echo.New()
	context.GET("/swagger/*", echoSwagger.WrapHandler)
	context.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowHeaders: []string{"*"},
		AllowMethods: []string{
			http.MethodGet,
			http.MethodHead,
			http.MethodPut,
			http.MethodPatch,
			http.MethodPost,
			http.MethodDelete,
		},
	}))

	//repositories
	loggingRepository := repository.Newlogs()
	novelRepository := repository.NewNovels()
	episodeRepository := repository.NewEpisodes()
	commentsRepository := repository.NewEpisodesComments()

	//dependenci injection
	graph := inject.Graph{}
	err := graph.Provide(
		&inject.Object{Value: utils.ConfigurationInstance},
		&inject.Object{Value: context},
		&inject.Object{Value: &server},
		&inject.Object{
			Value: &mediator,
			Name:  "default.mediator",
		},

		&inject.Object{Value: loggingRepository},
		&inject.Object{Value: novelRepository},
		&inject.Object{Value: episodeRepository},
		&inject.Object{Value: commentsRepository},
	)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
	if err := graph.Populate(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	//initializes the api server
	server.Run()

	fmt.Fprintln(os.Stdout, "server starting...")
	context.Logger.Fatal(context.Start(utils.ConfigurationInstance.Port))
	fmt.Fprintln(os.Stdout, "server stopped")

}
