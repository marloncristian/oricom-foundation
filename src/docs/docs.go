// GENERATED BY THE COMMAND ABOVE; DO NOT EDIT
// This file was generated by swaggo/swag

package docs

import (
	"bytes"
	"encoding/json"
	"strings"

	"github.com/alecthomas/template"
	"github.com/swaggo/swag"
)

var doc = `{
    "schemes": {{ marshal .Schemes }},
    "swagger": "2.0",
    "info": {
        "description": "{{.Description}}",
        "title": "{{.Title}}",
        "contact": {},
        "license": {
            "name": "Apache 2.0",
            "url": "http://www.apache.org/licenses/LICENSE-2.0.html"
        },
        "version": "{{.Version}}"
    },
    "host": "{{.Host}}",
    "basePath": "{{.BasePath}}",
    "paths": {
        "/novels/{novelid}/episodes": {
            "get": {
                "description": "Queries for novel episodes",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Episodes"
                ],
                "summary": "Novel episodes query",
                "operationId": "get-episodes",
                "parameters": [
                    {
                        "type": "integer",
                        "description": "Novel ID",
                        "name": "novelid",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "type": "array",
                            "items": {
                                "$ref": "#/definitions/models.GetEpisodeModel"
                            }
                        },
                        "headers": {
                            "Authorization": {
                                "type": "string",
                                "description": "Bearer token"
                            }
                        }
                    },
                    "400": {},
                    "403": {},
                    "404": {},
                    "500": {}
                }
            },
            "post": {
                "description": "Creates a new novel episode",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Episodes"
                ],
                "summary": "Creates novel episode",
                "operationId": "post-episodes",
                "parameters": [
                    {
                        "description": "Episode Model",
                        "name": "episode",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/models.PostEpisodeModel"
                        }
                    },
                    {
                        "type": "integer",
                        "description": "Novel ID",
                        "name": "novelid",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "201": {
                        "description": "Created",
                        "schema": {
                            "type": "array",
                            "items": {
                                "$ref": "#/definitions/models.GetEpisodeModel"
                            }
                        },
                        "headers": {
                            "Authorization": {
                                "type": "string",
                                "description": "Bearer token"
                            }
                        }
                    },
                    "400": {},
                    "403": {},
                    "404": {},
                    "500": {}
                }
            }
        },
        "/novels/{novelid}/episodes/{id}": {
            "get": {
                "description": "Return a specific novel episode",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Episodes"
                ],
                "summary": "Get a novel episode",
                "operationId": "get-episode",
                "parameters": [
                    {
                        "type": "integer",
                        "description": "Novel ID",
                        "name": "novelid",
                        "in": "path",
                        "required": true
                    },
                    {
                        "type": "integer",
                        "description": "Episode ID",
                        "name": "id",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/models.GetEpisodeModel"
                        },
                        "headers": {
                            "Authorization": {
                                "type": "string",
                                "description": "Bearer token"
                            }
                        }
                    },
                    "400": {},
                    "403": {},
                    "404": {},
                    "500": {}
                }
            },
            "delete": {
                "description": "Deletes a specific novel episode",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Episodes"
                ],
                "summary": "Deletes a specific novel episode",
                "operationId": "delete-episodes",
                "parameters": [
                    {
                        "type": "integer",
                        "description": "Novel ID",
                        "name": "novelid",
                        "in": "path",
                        "required": true
                    },
                    {
                        "type": "integer",
                        "description": "Episode ID",
                        "name": "id",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "204": {
                        "headers": {
                            "Authorization": {
                                "type": "string",
                                "description": "Bearer token"
                            }
                        }
                    },
                    "400": {},
                    "403": {},
                    "404": {},
                    "500": {}
                }
            },
            "patch": {
                "description": "Partially updates a specific novel episode",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Episodes"
                ],
                "summary": "Partially updates a specific novel episode",
                "operationId": "delete-episodes",
                "parameters": [
                    {
                        "type": "integer",
                        "description": "Novel ID",
                        "name": "novelid",
                        "in": "path",
                        "required": true
                    },
                    {
                        "type": "integer",
                        "description": "Episode ID",
                        "name": "id",
                        "in": "path",
                        "required": true
                    },
                    {
                        "description": "Episode Model",
                        "name": "episode",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/models.PatchEpisodeModel"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "type": "array",
                            "items": {
                                "$ref": "#/definitions/models.GetEpisodeModel"
                            }
                        },
                        "headers": {
                            "Authorization": {
                                "type": "string",
                                "description": "Bearer token"
                            }
                        }
                    },
                    "400": {},
                    "403": {},
                    "404": {},
                    "500": {}
                }
            }
        },
        "/novels/{novelid}/episodes/{id}/summary": {
            "get": {
                "description": "Requests novel episode summary",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Episodes"
                ],
                "summary": "Request novel episode summary",
                "operationId": "get-episode-summary",
                "parameters": [
                    {
                        "type": "integer",
                        "description": "Novel ID",
                        "name": "novelid",
                        "in": "path",
                        "required": true
                    },
                    {
                        "type": "integer",
                        "description": "Episode ID",
                        "name": "id",
                        "in": "path",
                        "required": true
                    },
                    {
                        "description": "Episode Model",
                        "name": "episode",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/models.PatchEpisodeModel"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "type": "array",
                            "items": {
                                "$ref": "#/definitions/models.GetEpisodeModel"
                            }
                        },
                        "headers": {
                            "Authorization": {
                                "type": "string",
                                "description": "Bearer token"
                            }
                        }
                    },
                    "400": {},
                    "403": {},
                    "404": {},
                    "500": {}
                }
            }
        }
    },
    "definitions": {
        "models.GetEpisodeAudioModel": {
            "type": "object",
            "properties": {
                "url": {
                    "type": "string"
                }
            }
        },
        "models.GetEpisodeAuthorModel": {
            "type": "object",
            "properties": {
                "id": {
                    "type": "string"
                },
                "name": {
                    "type": "string"
                }
            }
        },
        "models.GetEpisodeContributorsModel": {
            "type": "object",
            "properties": {
                "id": {
                    "type": "string"
                },
                "name": {
                    "type": "string"
                },
                "role": {
                    "type": "string"
                }
            }
        },
        "models.GetEpisodeCoverModel": {
            "type": "object",
            "properties": {
                "url": {
                    "type": "string"
                }
            }
        },
        "models.GetEpisodeModel": {
            "type": "object",
            "properties": {
                "audio": {
                    "type": "object",
                    "$ref": "#/definitions/models.GetEpisodeAudioModel"
                },
                "authors": {
                    "type": "array",
                    "items": {
                        "$ref": "#/definitions/models.GetEpisodeAuthorModel"
                    }
                },
                "contributors": {
                    "type": "array",
                    "items": {
                        "$ref": "#/definitions/models.GetEpisodeContributorsModel"
                    }
                },
                "cover": {
                    "type": "object",
                    "$ref": "#/definitions/models.GetEpisodeCoverModel"
                },
                "createdat": {
                    "type": "string"
                },
                "id": {
                    "type": "string"
                },
                "status": {
                    "type": "integer"
                },
                "synopsis": {
                    "type": "string"
                },
                "title": {
                    "type": "string"
                }
            }
        },
        "models.PatchEpisodeModel": {
            "type": "object",
            "required": [
                "title"
            ],
            "properties": {
                "authors": {
                    "type": "string"
                },
                "contributors": {
                    "type": "array",
                    "items": {
                        "$ref": "#/definitions/models.PostEpisodeContributorModel"
                    }
                },
                "cover": {
                    "type": "string"
                },
                "synopsis": {
                    "type": "string"
                },
                "title": {
                    "type": "string"
                }
            }
        },
        "models.PostEpisodeContributorModel": {
            "type": "object",
            "required": [
                "id",
                "role"
            ],
            "properties": {
                "id": {
                    "type": "string"
                },
                "role": {
                    "type": "string"
                }
            }
        },
        "models.PostEpisodeModel": {
            "type": "object",
            "required": [
                "title"
            ],
            "properties": {
                "authors": {
                    "type": "string"
                },
                "contributors": {
                    "type": "array",
                    "items": {
                        "$ref": "#/definitions/models.PostEpisodeContributorModel"
                    }
                },
                "cover": {
                    "type": "string"
                },
                "synopsis": {
                    "type": "string"
                },
                "title": {
                    "type": "string"
                }
            }
        }
    }
}`

type swaggerInfo struct {
	Version     string
	Host        string
	BasePath    string
	Schemes     []string
	Title       string
	Description string
}

// SwaggerInfo holds exported Swagger Info so clients can modify it
var SwaggerInfo = swaggerInfo{
	Version:     "1.0",
	Host:        "oricom-foundation.herokuapp.com",
	BasePath:    "/v1",
	Schemes:     []string{},
	Title:       "Oricom Foundation API",
	Description: "Foundation API for Oricom",
}

type s struct{}

func (s *s) ReadDoc() string {
	sInfo := SwaggerInfo
	sInfo.Description = strings.Replace(sInfo.Description, "\n", "\\n", -1)

	t, err := template.New("swagger_info").Funcs(template.FuncMap{
		"marshal": func(v interface{}) string {
			a, _ := json.Marshal(v)
			return string(a)
		},
	}).Parse(doc)
	if err != nil {
		return doc
	}

	var tpl bytes.Buffer
	if err := t.Execute(&tpl, sInfo); err != nil {
		return doc
	}

	return tpl.String()
}

func init() {
	swag.Register(swag.Name, &s{})
}
