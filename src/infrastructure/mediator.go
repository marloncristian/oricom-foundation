package infrastructure

import "reflect"

// MediatorHandler generic function for handling events
type MediatorHandler func(interface{})

// Mediator mediator contract
type Mediator interface {
	Handle(message interface{}, handler MediatorHandler)
	Raise(message interface{})
}

// MediatorImpl mediator concrete implementatino
type MediatorImpl struct {
	router map[string][]MediatorHandler
}

// Handle : adds handler to the internal map structure
func (m *MediatorImpl) Handle(message interface{}, handler MediatorHandler) {
	if m.router == nil {
		m.router = make(map[string][]MediatorHandler)
	}

	m.router[reflect.TypeOf(message).Name()] = []MediatorHandler{}
	m.router[reflect.TypeOf(message).Name()] = append(m.router[reflect.TypeOf(message).Name()], handler)
}

// Raise : raises an domain event
func (m *MediatorImpl) Raise(message interface{}) {
	if handlers, ok := m.router[reflect.TypeOf(message).Name()]; ok {
		for _, handler := range handlers {
			handler(message)
		}
	}
}
