package assertion

import (
	"fmt"
)

// AssertRootError root assertion error for base procedure failure
type AssertRootError struct {
	inner error
}

func (r AssertRootError) Error() string {
	return r.inner.Error()
}

// NewAssertRootError returns a new assert root error object
func NewAssertRootError(inner error) AssertRootError {
	return AssertRootError{
		inner: inner,
	}
}

// AssertFailure assertion failure
type AssertFailure struct {
	name    string
	failure error
}

// Assert assertion structure is resposable for chaining the assert functions
type Assert struct {
	entities   map[string]interface{}
	assertions []func() error
}

// Episode leaf for episode assertion
func (a *Assert) Episode() *EpisodeElement {
	return &EpisodeElement{
		root: a,
	}
}

// Novel leaf for novel assertion
func (a *Assert) Novel() *NovelElement {
	return &NovelElement{
		root: a,
	}
}

func (a *Assert) addEntity(name string, id string, entity interface{}) {
	if a.entities == nil {
		a.entities = make(map[string]interface{})
	}
	a.entities[fmt.Sprintf("%s:%s", name, id)] = entity
}

// GetEntity returns an entity previouslly created
func (a *Assert) GetEntity(name string, id string) interface{} {
	return a.entities[fmt.Sprintf("%s:%s", name, id)]
}

func (a *Assert) addAssertion(name string, assertion func() error) {
	a.assertions = append(a.assertions, assertion)
}

// Assert executes the assertion chain process
func (a *Assert) Assert() (err error) {
	for _, assertion := range a.assertions {
		err := assertion()
		if err != nil {
			return err
		}
	}
	return nil
}
