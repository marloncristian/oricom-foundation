package assertion

// NovelDoesNotExistsError error for novel not found
type NovelDoesNotExistsError struct {
}

func (e NovelDoesNotExistsError) Error() string {
	return "Novel does not exists"
}

// NovelNotOwnerError error for user not the owner
type NovelNotOwnerError struct {
}

func (e NovelNotOwnerError) Error() string {
	return "User is not the owner"
}
