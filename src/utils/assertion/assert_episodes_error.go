package assertion

// EpisodeDoesNotExistsError episode not found error
type EpisodeDoesNotExistsError struct {
}

func (e EpisodeDoesNotExistsError) Error() string {
	return "Episode does not exists"
}
