package assertion

import (
	"errors"
	"fmt"

	"github.com/marloncristian/oricom-goframework/database"
	"oricom.io/foundation/domain"
	"oricom.io/foundation/domain/repository"
)

// NovelElement novel assertion element
type NovelElement struct {
	root *Assert
}

// Root navigates back to root element
func (e *NovelElement) Root() *Assert {
	return e.root
}

// IsOwner verifies if the user is the owner of the novel
func (e *NovelElement) IsOwner(novelID string, userID string) *NovelElement {
	e.root.addAssertion(fmt.Sprintf("novel.%s.ownership", novelID), func() error {
		novelRoot := e.root.GetEntity("novel", novelID)
		if novelRoot == nil {
			novelRep := repository.NewNovels()
			novel, err := novelRep.GetByID(novelID)
			if err != nil {
				return NewAssertRootError(err)
			}
			e.root.addEntity("novel", novelID, &novel)
			novelRoot = novel
		}
		if novelRoot.(*domain.NovelEntity).Owner.UserID.Hex() != userID {
			return NovelNotOwnerError{}
		}
		return nil
	})
	return e
}

// Exists verifies if the novel exists
func (e *NovelElement) Exists(novelID string) *NovelElement {
	e.root.addAssertion(fmt.Sprintf("novel.%s.existence", novelID), func() error {
		novelRoot := e.root.GetEntity("novel", novelID)
		if novelRoot != nil {
			return nil
		}
		novelRep := repository.NewNovels()
		novel, err := novelRep.GetByID(novelID)
		if err != nil {
			if errors.Is(err, database.EntityNotFound{}) {
				return NovelDoesNotExistsError{}
			}
			return NewAssertRootError(err)
		}
		e.root.addEntity("novel", novelID, &novel)
		return nil
	})
	return e
}
