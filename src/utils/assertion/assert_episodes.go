package assertion

import (
	"errors"
	"fmt"

	"github.com/marloncristian/oricom-goframework/database"
	"oricom.io/foundation/domain/repository"
)

// EpisodeElement element for episode assertion
type EpisodeElement struct {
	root *Assert
}

// Root navigates back to the root element
func (e *EpisodeElement) Root() *Assert {
	return e.root
}

// Exists verifies if episode exists
func (e *EpisodeElement) Exists(episodeID string) *EpisodeElement {
	e.root.addAssertion(fmt.Sprintf("episode.%s.existence", episodeID), func() error {
		episodeRoot := e.root.GetEntity("episode", episodeID)
		if episodeRoot != nil {
			return nil
		}
		episodeRep := repository.NewEpisodes()
		episode, err := episodeRep.GetByID(episodeID)
		if err != nil {
			if errors.Is(err, database.EntityNotFound{}) {
				return EpisodeDoesNotExistsError{}
			}
			return NewAssertRootError(err)
		}
		e.root.addEntity("episode", episodeID, &episode)
		return nil
	})
	return e
}
