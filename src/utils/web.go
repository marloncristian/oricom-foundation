package utils

import (
	"crypto/md5"
	"encoding/base64"
	"encoding/hex"
	"errors"
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/marloncristian/oricom-goframework/database"
	_echo "github.com/marloncristian/oricom-goframework/web/echo"
	"oricom.io/foundation/domain/repository"
)

// ProducerRequired middleware for routes that requires the user to be a producer
func ProducerRequired(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {

		token, err := _echo.GetTokenFromHeader(c)
		if err != nil {
			return echo.ErrUnauthorized
		}
		rep := repository.NewProducers()
		_, errPrd := rep.GetByID(token.Subscriber)
		if errPrd != nil {
			if _, ok := errPrd.(database.EntityNotFound); ok {
				return c.JSON(http.StatusUnauthorized, ToEntityViolation(errors.New("Not a producer")))
			} else {
				return c.JSON(http.StatusBadRequest, ToEntityViolation(errPrd))
			}
		}
		return next(c)
	}
}

// UnauthorizedError returns an unauthorized resopnse message
func UnauthorizedError(c echo.Context, err IError) error {
	return c.JSON(http.StatusUnauthorized, ToEntityViolation(err))
}

// InternalServerError returns an internal server error response message
func InternalServerError(c echo.Context, err IError) error {
	return c.JSON(http.StatusInternalServerError, ToEntityViolation(err))
}

// BadRequestError returns a bad request error message
func BadRequestError(c echo.Context, err IError) error {
	return c.JSON(http.StatusBadRequest, ToEntityViolation(err))
}

// NoContent returns a simple no content response
func NoContent(c echo.Context) error {
	return c.NoContent(http.StatusNoContent)
}

// GetMD5FromBase64 returns the md5 of a base64 string
func GetMD5FromBase64(param string) (res string, err error) {
	data, err := base64.StdEncoding.DecodeString(param)
	hash := md5.New()
	hash.Write(data)
	res = hex.EncodeToString(hash.Sum(nil))
	return
}

// GetMD5FromString returns the md5 from a given string
func GetMD5FromString(param string) (res string, err error) {
	hash := md5.New()
	hash.Write([]byte(param))
	res = hex.EncodeToString(hash.Sum(nil))
	return
}

// GetMD5FromByteArray returns the md5 from a given byte array
func GetMD5FromByteArray(bytes []byte) (res string, err error) {
	hash := md5.New()
	hash.Write(bytes)
	res = hex.EncodeToString(hash.Sum(nil))
	return
}
