package utils

import (
	"fmt"

	"github.com/marloncristian/oricom-goframework/core"
)

// Configuration structure
type Configuration struct {
	ServerURL        string
	DatabaseName     string
	DatabaseUsername string
	DatabasePassword string
	Port             string
	Security         SecurityConfiguration
	Services         ServicesConfiguration
}

// SecurityConfiguration security configuration struct
type SecurityConfiguration struct {
	Secret string
}

// ServicesConfiguration services configuration struct
type ServicesConfiguration struct {
	Authentication ServicesAuthenticationConfiguration
	Endpoints      map[string]string
}

// ServicesAuthenticationConfiguration services authentication struct
type ServicesAuthenticationConfiguration struct {
	Username     string
	Password     string
	ClientID     string
	ClientSecret string
}

// GetServerURL returns the server url
func (configuration Configuration) GetServerURL() string {
	if configuration.DatabaseUsername != "" && configuration.DatabasePassword != "" {
		//return fmt.Sprintf("mongodb://%s:%s@%s:%s/%s?retryWrites=false",
		return fmt.Sprintf("mongodb+srv://%s:%s@%s/%s?retryWrites=true&w=majority",
			configuration.DatabaseUsername,
			configuration.DatabasePassword,
			configuration.ServerURL,
			configuration.DatabaseName,
		)
	}
	return fmt.Sprintf("mongodb://%s",
		configuration.ServerURL)
}

// ConfigurationInstance instance for application configurations
var ConfigurationInstance *Configuration

func init() {
	ConfigurationInstance = &Configuration{}
	core.ParseConfiguration(&ConfigurationInstance)
}
