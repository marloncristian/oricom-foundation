package utils

import "gopkg.in/go-playground/validator.v9"

// IError error type interface
type IError interface {
	Error() string
}

// ToEntityViolation converts an generic model to a default error response
func ToEntityViolation(err IError) interface{} {
	if errs, ok := err.(validator.ValidationErrors); ok {
		violMap := make(map[string]EntityViolationItemErrorModel)
		for _, value := range errs {
			violMap[value.Field()] = EntityViolationItemErrorModel{
				Rule:  value.Tag(),
				Value: value.Param(),
			}
		}
		return EntityViolationErrorModel{
			Error:      "Input Validation",
			Violations: violMap,
		}
	}
	return ErrorModel{
		Error: err.Error(),
	}
}

// EntityViolationErrorModel model for api error output
type EntityViolationErrorModel struct {
	Error      string                                   `json:"error"`
	Violations map[string]EntityViolationItemErrorModel `json:"violations"`
}

// EntityViolationItemErrorModel model for api error output
type EntityViolationItemErrorModel struct {
	Rule  string `json:"rule"`
	Value string `json:"value,omitempty"`
}

// ErrorModel default error structure
type ErrorModel struct {
	Error string `json:"error"`
}
