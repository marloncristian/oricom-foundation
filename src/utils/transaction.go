package utils

// TransactionFunc method for rollback procedure
type TransactionFunc func()

// Transaction structure
type Transaction struct {
	Vote            bool
	RollbackMethods []TransactionFunc
	CommitMethods   []TransactionFunc
}

// NewTransaction creates a new transaction
func NewTransaction() Transaction {
	return Transaction{
		Vote: false,
	}
}

// AddRollback adds rollback procedure
func (t *Transaction) AddRollback(rollback TransactionFunc) {
	t.RollbackMethods = append(t.RollbackMethods, rollback)
}

// AddCommit adds commit procedure
func (t *Transaction) AddCommit(commit TransactionFunc) {
	t.CommitMethods = append(t.CommitMethods, commit)
}

// Defer transaction confirmation or rollback
func (t *Transaction) Defer() {
	if !t.Vote {
		for _, fn := range t.RollbackMethods {
			fn()
		}
	} else {
		for _, fn := range t.CommitMethods {
			fn()
		}
	}
}

// Commit commit method
func (t *Transaction) Commit() {
	t.Vote = true
}
