package utils

import "strings"

// SplitAndTrim split a string and trim the elements
func SplitAndTrim(s string, sep string) []string {
	var res []string
	spl := strings.Split(s, ",")
	for _, item := range spl {
		if len(item) > 0 {
			res = append(res, strings.Trim(item, " "))
		}
	}
	return res
}
