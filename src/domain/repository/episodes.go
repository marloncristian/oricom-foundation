package repository

import (
	"time"

	"github.com/marloncristian/oricom-goframework/database"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"oricom.io/foundation/domain"
)

// Episodes repository for managing episodes
type Episodes struct {
	base database.RepositoryBase
}

// NewEpisodes creates a new instance of episodes repository
func NewEpisodes() *Episodes {
	return &Episodes{
		base: database.NewRepositoryBase("episodes"),
	}
}

// Create creates a new novel
func (rep *Episodes) Create(novel domain.NovelEpisodeEntity) (primitive.ObjectID, error) {
	novel.CreatedAt = time.Now()
	novel.UpdatedAt = time.Now()
	novel.Status = domain.NovelStatus.Draft
	return rep.base.InsertOne(novel)
}

// Update updates episodes entity
func (rep *Episodes) Update(id string, entity map[string]interface{}) error {
	objID, objErr := primitive.ObjectIDFromHex(id)
	if objErr != nil {
		return objErr
	}

	entity["updatedat"] = time.Now()
	if upErr := rep.base.UpdateOne(objID, bson.M{"$set": entity}, nil); upErr != nil {
		return upErr
	}

	return nil
}

// UpdateCommentsCount increments the total count of comments
func (rep *Episodes) UpdateCommentsCount(id string, vector int) 	(err error) {
	objID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return
	}

	update := bson.D{
		primitive.E{Key: "$inc", Value: bson.D{
			primitive.E{Key: "summary.comments", Value: vector},
		}},
	}

	err = rep.base.UpdateOne(objID, update, nil)
	return
}

// Replace replaces an entity completely
func (rep *Episodes) Replace(entity domain.NovelEpisodeEntity) (err error) {
	err = rep.base.ReplaceOne(entity.ID, entity)
	return
}

// GetByID gets the episode by string id
func (rep *Episodes) GetByID(id string) (res domain.NovelEpisodeEntity, err error) {
	obj, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return
	}

	err = rep.base.GetOne(bson.M{"_id": obj}, &res)
	return
}

// GetByNovelID returns all episodes by novelid
func (rep *Episodes) GetByNovelID(novelID string) (res []domain.NovelEpisodeEntity, err error) {
	novelIDObj, err := primitive.ObjectIDFromHex(novelID)
	if err != nil {
		return
	}

	pipeline := bson.M{"$match": bson.M{"novelid": novelIDObj}}
	err = rep.base.Aggregate([]bson.M{pipeline}, &res)
	return
}

// Delete removes a certain episode
func (rep Episodes) Delete(id string) (err error) {
	objID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return
	}

	err = rep.base.DeleteOne(objID)
	return
}
