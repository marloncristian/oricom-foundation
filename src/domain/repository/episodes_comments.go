package repository

import (
	"time"

	"github.com/marloncristian/oricom-goframework/database"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"oricom.io/foundation/domain"
)

// EpisodesComments repository for managing episodes
type EpisodesComments struct {
	base database.RepositoryBase
}

// NewEpisodesComments creates a new instance of episodes repository
func NewEpisodesComments() *EpisodesComments {
	return &EpisodesComments{
		base: database.NewRepositoryBase("episodes_comments"),
	}
}

// Create creates a new novel
func (rep *EpisodesComments) Create(comment domain.NovelEpisodeCommentEntity) (primitive.ObjectID, error) {
	comment.CreatedAt = time.Now()
	comment.UpdatedAt = time.Now()
	return rep.base.InsertOne(comment)
}

// Update updates episodes entity
func (rep *EpisodesComments) Update(id string, entity map[string]interface{}) error {
	objID, objErr := primitive.ObjectIDFromHex(id)
	if objErr != nil {
		return objErr
	}

	entity["updatedat"] = time.Now()
	if upErr := rep.base.UpdateOne(objID, bson.M{"$set": entity}, nil); upErr != nil {
		return upErr
	}

	return nil
}

// GetByID gets the episode by string id
func (rep *EpisodesComments) GetByID(id string) (res domain.NovelEpisodeCommentEntity, err error) {
	obj, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return
	}

	err = rep.base.GetOne(bson.M{"_id": obj}, &res)
	return
}

// CountFiveMinutesComments returns a count of all comments made by an user to an episode
func (rep *EpisodesComments) CountFiveMinutesComments(episodeID primitive.ObjectID, userID primitive.ObjectID) (int, error) {
	pipeline := []bson.M{
		{"$match": bson.M{"episodeid": episodeID, "createdat": bson.M{"$gt": time.Now().Add(-time.Minute * 5)}, "user._id": userID}},
		{"$group": bson.M{"_id": nil, "count": bson.M{"$sum": 1}}},
		{"$project": bson.M{"_id": 0}},
	}

	result := []struct {
		Count int
	}{}

	err := rep.base.Aggregate(pipeline, &result)
	if err != nil {
		return 0, err
	}

	if len(result) == 0 {
		return 0, nil
	}

	return result[0].Count, nil
}

// GetByEpisodeID returns all episodes by novelid
func (rep *EpisodesComments) GetByEpisodeID(novelID string, skip int64, limit int64) (res []domain.NovelEpisodeCommentEntity, err error) {
	novelIDObj, err := primitive.ObjectIDFromHex(novelID)
	if err != nil {
		return
	}

	pipeline := []bson.M{
		{"$match": bson.M{"novelepisodeid": novelIDObj, "bounds": nil}},
		{"$skip": skip},
		{"$limit": limit},
	}
	err = rep.base.Aggregate(pipeline, &res)
	return
}

// Delete removes a certain episode
func (rep EpisodesComments) Delete(id string) (err error) {
	objID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return
	}

	err = rep.base.DeleteOne(objID)
	return
}
