package repository

import (
	"time"

	"github.com/labstack/gommon/log"
	"github.com/marloncristian/oricom-goframework/database"
	"oricom.io/foundation/domain"
)

// Logs repository for logs
type Logs struct {
	base database.RepositoryBase
}

// Newlogs creates a new instance of episodes repository
func Newlogs() *Logs {
	return &Logs{
		base: database.NewRepositoryBase("logs"),
	}
}

// Error creates a error log
func (rep *Logs) Error(content string) {
	logEntity := domain.LogEntity{
		CreatedAt: time.Now(),
		Type:      domain.LogType.Error,
		Content:   content,
	}
	_, err := rep.base.InsertOne(logEntity)
	if err != nil {
		log.Printf("Error creating Error. Original: %s. Error: %s", content, err.Error())
		log.Error(err)
	}
}

// Warning creates a warning log
func (rep *Logs) Warning(content string) {
	logEntity := domain.LogEntity{
		CreatedAt: time.Now(),
		Type:      domain.LogType.Error,
		Content:   content,
	}
	_, err := rep.base.InsertOne(logEntity)
	if err != nil {
		log.Printf("Error creating Warning. Original: %s. Error: %s", content, err.Error())
		log.Error(err)
	}
}
