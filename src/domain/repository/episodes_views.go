package repository

import (
	"time"

	"github.com/marloncristian/oricom-goframework/database"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"oricom.io/foundation/domain"
)

// EpisodesViews repository for managing episodes
type EpisodesViews struct {
	base database.RepositoryBase
}

// NewEpisodesViews creates a new instance of episodes views repository
func NewEpisodesViews() EpisodesViews {
	return EpisodesViews{
		base: database.NewRepositoryBase("episodes_views"),
	}
}

// Create creates a new novel
func (rep *EpisodesViews) Create(address string) (primitive.ObjectID, error) {
	return rep.base.InsertOne(domain.NovelEpisodeViewsEntity{
		Address:   address,
		CreatedAt: time.Now(),
	})
}
