package repository

import (
	"reflect"
	"time"

	"github.com/marloncristian/oricom-goframework/database"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"oricom.io/foundation/domain"
)

// Novels repository for managing producers
type Novels struct {
	base database.RepositoryBase
}

// NewNovels creates a new instance of service producers
func NewNovels() *Novels {
	return &Novels{
		base: database.NewRepositoryBase("novels"),
	}
}

// Create creates a new novel
func (rep Novels) Create(novel domain.NovelEntity) (primitive.ObjectID, error) {
	novel.CreatedAt = time.Now()
	novel.UpdatedAt = time.Now()
	novel.Status = domain.NovelStatus.Draft
	return rep.base.InsertOne(novel)
}

// GetByID gets the novel by string id
func (rep Novels) GetByID(id string) (domain.NovelEntity, error) {
	res := domain.NovelEntity{}
	obj, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return res, err
	}

	err = rep.base.GetOne(bson.M{"_id": obj}, &res)
	if err != nil {
		return res, err
	}
	return res, nil
}

// GetFromOwner returns an range of novels for a single owner
func (rep Novels) GetFromOwner(ownerID string, skip int64, limit int64) ([]domain.NovelEntity, error) {
	res := []domain.NovelEntity{}

	objID, errObj := primitive.ObjectIDFromHex(ownerID)
	if errObj != nil {
		return nil, errObj
	}

	query := bson.M{
		"owner._id": objID,
	}
	errGet := rep.base.GetWithSkipLimit(query, &res, skip, limit)
	if errGet != nil {
		return nil, errGet
	}

	return res, nil
}

// GetAllTitles Returns all titles from database to be used in suggestions
func (rep Novels) GetAllTitles() ([]domain.GetSuggestionsNovels, error) {
	res := []domain.GetSuggestionsNovels{}

	pipeline := []bson.M{
		bson.M{
			"$project": bson.M{"title": 1},
		},
	}

	errGet := rep.base.Aggregate(pipeline, &res)
	if errGet != nil {
		return nil, errGet
	}

	return res, nil
}

// Search returns a full text search of novels on title, synopsis, tags e owner.name
func (rep Novels) Search(q string, skip int64, limit int64) ([]domain.NovelEntity, error) {
	res := []domain.NovelEntity{}

	pipeline := []bson.M{
		bson.M{
			"$searchBeta": bson.M{
				"search":    bson.M{"query": q, "path": bson.A{"title", "synopsis", "owner.name", "tags"}},
				"highlight": bson.M{"path": "title"},
			},
		},
		bson.M{
			"$sort": bson.M{"createdat": -1},
		},
		bson.M{"$limit": limit},
		bson.M{"$skip": skip},
	}

	errGet := rep.base.Aggregate(pipeline, &res)
	if errGet != nil {
		return nil, errGet
	}

	return res, nil
}

// Delete removes a certain novel
func (rep Novels) Delete(id string) error {
	objID, errObj := primitive.ObjectIDFromHex(id)
	if errObj != nil {
		return errObj
	}

	errDel := rep.base.DeleteOne(objID)
	if errDel != nil {
		return errDel
	}

	//TODO: remove all episodes
	return nil
}

// Update updates novel entity
func (rep Novels) Update(id string, entity map[string]interface{}) error {
	objID, objErr := primitive.ObjectIDFromHex(id)
	if objErr != nil {
		return objErr
	}

	novel, getErr := rep.GetByID(id)
	if getErr != nil {
		return getErr
	}

	modified := false
	if title, ok := entity["Title"]; ok && title.(string) != novel.Title {
		modified = true
	}
	if synopsis, ok := entity["Synopsis"]; ok && synopsis.(string) != novel.Synopsis {
		modified = true
	}
	if tags, ok := entity["Tags"]; ok && !reflect.DeepEqual(tags.([]string), novel.Tags) {
		modified = true
	}
	if status, ok := entity["Status"]; ok && status.(int32) != novel.Status {
		modified = true
	}
	if !modified {
		return domain.EntityNotModifiedError{}
	}

	entity["UpdatedAt"] = time.Now()
	if upErr := rep.base.UpdateOne(objID, bson.M{"$set": entity}, nil); upErr != nil {
		return upErr
	}

	return nil
}
