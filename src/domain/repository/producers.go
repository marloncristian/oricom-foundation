package repository

import (
	"time"

	"github.com/marloncristian/oricom-goframework/database"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"oricom.io/foundation/domain"
)

// Producers repository for managing producers
type Producers struct {
	base database.RepositoryBase
}

// NewProducers creates a new instance of service producers
func NewProducers() Producers {
	return Producers{
		base: database.NewRepositoryBase("producers"),
	}
}

// GetAll returns all producers in database
func (rep Producers) GetAll() ([]domain.ProducerEntity, error) {
	res := []domain.ProducerEntity{}
	err := rep.base.GetAll(&res)
	if err != nil {
		return nil, err
	}
	return res, nil
}

// GetAllWithSkipLimit returns all producers in database
func (rep Producers) GetWithSkipLimit(query interface{}, skip int64, limit int64) ([]domain.ProducerEntity, error) {
	res := []domain.ProducerEntity{}
	err := rep.base.GetWithSkipLimit(query, &res, skip, limit)
	if err != nil {
		return nil, err
	}
	return res, nil
}

// GetByID gets the user by string id
func (rep Producers) GetByID(id string) (domain.ProducerEntity, error) {
	res := domain.ProducerEntity{}
	obj, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return res, err
	}

	err = rep.base.GetOne(bson.M{"_id": obj}, &res)
	if err != nil {
		return res, err
	}
	return res, nil
}

// Create creates a new producer
func (rep Producers) Create(producer domain.ProducerEntity) (primitive.ObjectID, error) {
	producer.CreatedAt = time.Now()
	return rep.base.InsertOne(producer)
}

// Update updates the property displayname only
func (rep Producers) Update(id string, entity map[string]interface{}) error {
	objID, objErr := primitive.ObjectIDFromHex(id)
	if objErr != nil {
		return objErr
	}

	prc, getErr := rep.GetByID(id)
	if getErr != nil {
		return getErr
	}

	displayName, dsOk := entity["DisplayName"]
	status, stOk := entity["Status"]
	if (!dsOk || displayName.(string) == prc.DisplayName) && (!stOk && status.(int32) == prc.Status) {
		return domain.EntityNotModifiedError{}
	}

	if upErr := rep.base.UpdateOne(objID, bson.M{"$set": entity}, nil); upErr != nil {
		return upErr
	}

	return nil
}
