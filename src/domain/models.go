package domain

// GetSuggestionsNovels model for novel suggestions
type GetSuggestionsNovels struct {
	Title string `json:"title"`
}

// SecurityTokenModel : model for authentication verification
type SecurityTokenModel struct {
	AccessToken string `json:"access_token"`
}

// UserModel : simple user model
type UserModel struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}
