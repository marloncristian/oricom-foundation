package handlers

import (
	"fmt"

	"oricom.io/foundation/domain/events"
	"oricom.io/foundation/domain/repository"
)

// CommentCreatedHandler comment created handler
func CommentCreatedHandler(message interface{}) {
	episodeRepository := repository.Episodes{}
	loggingRepository := repository.Logs{}

	commentCreatedMessage := message.(events.CommentCreatedMessage)
	err := episodeRepository.UpdateCommentsCount(commentCreatedMessage.EpisodeID.Hex(), 1)
	if err != nil {
		loggingRepository.Error(fmt.Sprintln("Error processging CommentCreated message: ", err))
	}
}
