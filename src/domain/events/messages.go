package events

import "go.mongodb.org/mongo-driver/bson/primitive"

// CommentCreatedMessage new comment message
type CommentCreatedMessage struct {
	EpisodeID primitive.ObjectID
}
