package domain

// EntityNotModifiedError generic entity not modified error
type EntityNotModifiedError struct {
	description string
}

// Error generic description error method
func (err EntityNotModifiedError) Error() string {
	return "Entity not modified"
}

// EntityInvalidStatusError generic entity invalid status error
type EntityInvalidStatusError struct {
	description string
}

// Error generic description error method
func (err EntityInvalidStatusError) Error() string {
	return "Invalid Status"
}
