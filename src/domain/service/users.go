package service

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/marloncristian/oricom-goframework/web/authentication"
	"oricom.io/foundation/domain"
	"oricom.io/foundation/utils"
)

// Users : Service for interaction with security.users resources
//IMPORTANT: this is a temporary struct, in the future this implementation will be transform in
//a GRPC call and this will be erased
type Users struct {
	token         *authentication.AuthenticationToken
	originalToken string
}

func (users *Users) tokenInvalid() (bool, error) {
	if users.token == nil {
		return true, nil
	}
	expired, err := users.token.Expired()
	if err != nil {
		return false, err
	}
	return expired, nil
}

func (users *Users) getToken() (*authentication.AuthenticationToken, error) {
	invalid, err := users.tokenInvalid()
	if err != nil {
		return nil, fmt.Errorf("Security service error: %s", err.Error())
	}
	if invalid {
		body := fmt.Sprintf("grant_type=password&username=%s&password=%s&client_id=%s&client_secret=%s",
			utils.ConfigurationInstance.Services.Authentication.Username,
			utils.ConfigurationInstance.Services.Authentication.Password,
			utils.ConfigurationInstance.Services.Authentication.ClientID,
			utils.ConfigurationInstance.Services.Authentication.ClientSecret,
		)
		endpoint := fmt.Sprintf("%s/auth", utils.ConfigurationInstance.Services.Endpoints["Security"])
		req, errRequesting := http.NewRequest("POST", endpoint, bytes.NewBufferString(body))
		if errRequesting != nil {
			return nil, fmt.Errorf("Security service error: %s", errRequesting.Error())
		}

		client := &http.Client{}
		resp, errGetTokn := client.Do(req)
		if errGetTokn != nil {
			return nil, fmt.Errorf("Security service error: %s", errGetTokn.Error())
		}
		if resp.StatusCode != 200 {
			return nil, fmt.Errorf("Security service error (%d)", resp.StatusCode)
		}

		model := domain.SecurityTokenModel{}
		errDecoding := json.NewDecoder(resp.Body).Decode(&model)
		if errDecoding != nil {
			return nil, fmt.Errorf("Security service error: %s", errDecoding.Error())
		}

		users.token = &authentication.AuthenticationToken{}
		users.token.Decode(model.AccessToken)
	}
	return users.token, nil
}

// GetByID : Returns a certain user by ID
func (users *Users) GetByID(ID string) (domain.UserModel, error) {
	endpoint := fmt.Sprintf("%s/v1/users/%s", utils.ConfigurationInstance.Services.Endpoints["Security"], ID)
	req, err := http.NewRequest("GET", endpoint, nil)
	if err != nil {
		return domain.UserModel{}, err
	}

	token, errTkn := users.getToken()
	if errTkn != nil {
		return domain.UserModel{}, errTkn
	}
	req.Header.Add("Authorization", fmt.Sprintf("bearer %s", token.Source))

	client := &http.Client{}
	resp, errGetUsr := client.Do(req)
	if errGetUsr != nil {
		return domain.UserModel{}, errGetUsr
	}
	if resp.StatusCode != 200 {
		return domain.UserModel{}, fmt.Errorf("Security service error (%d)", resp.StatusCode)
	}

	model := domain.UserModel{}
	errDec := json.NewDecoder(resp.Body).Decode(&model)
	if errDec != nil {
		return domain.UserModel{}, errDec
	}

	return model, nil
}
