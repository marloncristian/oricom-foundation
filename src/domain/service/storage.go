package service

import (
	"bytes"
	"fmt"
	"image"
	"image/jpeg"
	"net/url"

	"golang.org/x/net/context"
	"golang.org/x/oauth2/google"
	storage "google.golang.org/api/storage/v1"
)

const (
	scope      = storage.DevstorageFullControlScope
	bucket     = "evident-torus-273114.appspot.com"
	storageURL = "https://storage.googleapis.com"

	high   = 500
	medium = 310
	low    = 120
)

// Storage struct for storage interaction
type Storage struct {
}

// UpdloadedImages model for uploaded image
type UpdloadedImages struct {
	Hash string
	Link string
}

// UploadImageBase64 uploads files to storage service
func (s *Storage) UploadImageBase64(base string, name string) (string, error) {

	i, err := NewImage(base)
	if err != nil {
		return "", err
	}

	link, err := s.UploadImage(i.original, name)
	if err != nil {
		return "", err
	}

	return link, nil
}

// UploadBytes uploads a byte array
func (s *Storage) UploadBytes(content []byte, name string) (ling string, err error) {

	client, err := google.DefaultClient(context.Background(), scope)
	if err != nil {
		return "", err
	}

	service, err := storage.New(client)
	if err != nil {
		return "", err
	}

	reader := bytes.NewReader(content)

	object := &storage.Object{Name: name}
	_, err = service.Objects.Insert(bucket, object).Media(reader).Do()
	if err != nil {
		return "", err
	}

	return fmt.Sprintf("%s/%s/%s", storageURL, bucket, url.PathEscape(name)), nil
}

// UploadImage uploads an image
func (s *Storage) UploadImage(img image.Image, name string) (link string, err error) {
	buf := new(bytes.Buffer)
	err = jpeg.Encode(buf, img, nil)
	if err != nil {
		return "", err
	}
	return s.UploadBytes(buf.Bytes(), name)
}

// DeleteFile deletes a image
func (s *Storage) DeleteFile(name string) (err error) {

	client, err := google.DefaultClient(context.Background(), scope)
	if err != nil {
		return
	}

	service, err := storage.New(client)
	if err != nil {
		return
	}

	err = service.Objects.Delete(bucket, name).Do()
	if err != nil {
		return
	}

	return nil
}
