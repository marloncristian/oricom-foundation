package service

import (
	"bytes"
	"crypto/md5"
	"encoding/base64"
	"encoding/hex"
	"image"
	_ "image/jpeg"
	_ "image/png"

	"github.com/nfnt/resize"
	"github.com/oliamb/cutter"
)

// Image struct for images
type Image struct {
	original image.Image
	hash     string
}

// NewImage creates a new image
func NewImage(base string) (*Image, error) {

	data, err := base64.StdEncoding.DecodeString(base)
	if err != nil {
		return nil, err
	}

	original, _, err := image.Decode(bytes.NewReader(data))
	if err != nil {
		return nil, err
	}

	h := md5.New()
	h.Write(data)
	hashMD5 := hex.EncodeToString(h.Sum(nil))

	return &Image{
		hash:     hashMD5,
		original: original,
	}, nil
}

// GetSize returns the size of the image
func (image *Image) GetSize() (int, int) {
	bounds := image.original.Bounds()
	return bounds.Max.X, bounds.Max.Y
}

// Crop crops the image
func (image *Image) Crop(width int, height int) *Image {
	image.original, _ = cutter.Crop(image.original, cutter.Config{
		Width:  width,
		Height: height,
		Mode:   cutter.Centered,
	})
	return image
}

// Resize resizes the image
func (image *Image) Resize(vector uint) *Image {
	bound := image.original.Bounds()
	if bound.Max.X > bound.Max.Y {
		ratio := float64(bound.Max.X) / float64(bound.Max.Y)
		x := uint(float64(vector) * ratio)
		image.original = resize.Resize(x, vector, image.original, resize.Lanczos3)
	} else {
		ratio := float64(bound.Max.Y) / float64(bound.Max.X)
		y := uint(float64(vector) * ratio)
		image.original = resize.Resize(vector, y, image.original, resize.Lanczos3)
	}
	return image
}
