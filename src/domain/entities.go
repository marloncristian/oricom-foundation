package domain

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

// UserEntity shared entity for user
type UserEntity struct {
	UserID primitive.ObjectID `bson:"_id,omitempty" json:"id"`
	Name   string
}

// NovelEntity entity for novel
type NovelEntity struct {
	ID        primitive.ObjectID `bson:"_id,omitempty" json:"id"`
	Title     string
	Synopsis  string
	Status    int32
	Tags      []string
	CreatedAt time.Time
	UpdatedAt time.Time
	Owner     UserEntity
	CoverURL  NovelCoverFile
	Rank      int32
}

// NovelCoverFile structure
type NovelCoverFile struct {
	LowQualityFileID    string `json:"low"`
	MediumQualityFileID string `json:"medium"`
	HighQualityFileID   string `json:"high"`
}

// NovelStatus novel status
var NovelStatus = struct {
	Unknown    int32
	Draft      int32
	Publishing int32
	Published  int32
	Blocked    int32
}{
	0, 1, 2, 3, 4,
}

// NovelStatusChangedEntity records the status change of the novel
type NovelStatusChangedEntity struct {
	ID        primitive.ObjectID `bson:"_id,omitempty" json:"id"`
	NovelID   primitive.ObjectID `json:"novel"`
	Status    int64
	UpdatedAt time.Time
}

// NovelSummaryEntity summary entity for novel
type NovelSummaryEntity struct {
	Views    int64
	Episodes int32
	Comments int64
}

// NovelEpisodeEntity segment of the novel
type NovelEpisodeEntity struct {
	ID           primitive.ObjectID `bson:"_id,omitempty" json:"id"`
	NovelID      primitive.ObjectID `json:"novel"`
	Title        string
	Synopsis     string
	Status       int32
	CreatedAt    time.Time
	UpdatedAt    time.Time
	Authors      []UserEntity
	Contributors []ContributorEntity
	Cover        *EpisodeCover
	Audio        *EpisodeAudio
	Summary      *NovelEpisodeSummary
	Body         *EpisodeBody
}

// NovelEpisodeLovesEntity struct for storing the relation between user and episode
type NovelEpisodeLovesEntity struct {
	UserID    primitive.ObjectID
	EpisodeID primitive.ObjectID
}

// NovelEpisodeViewsEntity struct for storing the views for an specific episode
type NovelEpisodeViewsEntity struct {
	Address   string
	CreatedAt time.Time
}

// NovelEpisodeStatus novel episode status
var NovelEpisodeStatus = struct {
	Unknown    int32
	Draft      int32
	Publishing int32
	Published  int32
}{
	0, 1, 2, 3,
}

// EpisodeBody data struct for data
//	hash is a SHA1 representation
type EpisodeBody struct {
	Hash string
	Data string
}

// EpisodeAudio structure
type EpisodeAudio struct {
	Hash     string
	URL      string
	Redirect string
}

// EpisodeCover structure
type EpisodeCover struct {
	Hash     string
	URL      string
	Redirect string
}

// NovelEpisodeCommentEntity comments for episode
type NovelEpisodeCommentEntity struct {
	ID        primitive.ObjectID `bson:"_id,omitempty" json:"id"`
	EpisodeID primitive.ObjectID
	User      UserEntity
	Comment   string
	CreatedAt time.Time
	UpdatedAt time.Time
}

// NovelEpisodeSummary summary data from an specific novel episode
type NovelEpisodeSummary struct {
	Views    int64
	Comments int64
	Loves    int64
}

// ContributorEntity shared contributor entity
type ContributorEntity struct {
	UserID primitive.ObjectID `bson:"_id,omitempty" json:"id"`
	Role   string
	Name   string
}

// ProducerEntity relates to content producers. this entity is an one-to-to
// to [security].user by ID.
type ProducerEntity struct {
	ID          primitive.ObjectID `bson:"_id,omitempty" json:"id"`
	DisplayName string             `json:"displayname"`
	Status      int32              `json:"status"`
	CreatedAt   time.Time          `json:"createdat"`
	ActivatedAt *time.Time         `json:"activatedat"`
}

// ProducerStatus producer status
var ProducerStatus = struct {
	Unknown    int32
	Activating int32
	Active     int32
	Inactive   int32
}{
	0, 1, 2, 3,
}

// LogEntity ia a commom element for logging informations
type LogEntity struct {
	ID        primitive.ObjectID `bson:"_id,omitempty" json:"id"`
	Type      int32              `json:"type"`
	Content   string             `json:"content"`
	CreatedAt time.Time          `json:"createdat"`
}

// LogType logging type information
var LogType = struct {
	Error   int32
	Warning int32
}{
	0, 1,
}
